package main.sparrow.com.premierleague.domain;

import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import main.sparrow.com.premierleague.entities.matches.MatchesItem;
import kotlin.Pair;

import static junit.framework.TestCase.assertEquals;

public class FindClosestDateTest {

    private List <Pair <String, List <MatchesItem>>> matches;
    private FindClosestDate findClosestDate = new FindClosestDate ();
    private SimpleDateFormat format = new SimpleDateFormat ( "yyyy-MM-dd" );

    @Before public void setUp() throws FileNotFoundException {
        Pair <String, List <MatchesItem>> pair1 = new Pair <> ( "2018-12-15" , new ArrayList <> () );
        Pair <String, List <MatchesItem>> pair2 = new Pair <> ( "2018-12-17" , new ArrayList <> () );
        Pair <String, List <MatchesItem>> pair3 = new Pair <> ( "2018-12-18" , new ArrayList <> () );
        Pair <String, List <MatchesItem>> pair4 = new Pair <> ( "2018-12-19" , new ArrayList <> () );
        Pair <String, List <MatchesItem>> pair5 = new Pair <> ( "2018-12-25" , new ArrayList <> () );

        matches = new ArrayList <> ();
        matches.add ( pair1 );
        matches.add ( pair2 );
        matches.add ( pair3 );
        matches.add ( pair4 );
        matches.add ( pair5 );
    }

    @Test public void with15_12_sholudReturn_0() throws ParseException {
        int result = findClosestDate.invoke ( matches , format.parse ( "2018-12-15" ) );

        assertEquals(0 , result);
    }

    @Test public void with17_12_sholudReturn_1() throws ParseException {
        int result = findClosestDate.invoke ( matches , format.parse ( "2018-12-17" ) );

        assertEquals(1 , result);

    }

    @Test public void with24_12_sholudReturn_4() throws ParseException {
        int result = findClosestDate.invoke ( matches , format.parse ( "2018-12-24" ) );

        assertEquals(4 , result);

    }

    @Test public void with1_12_sholudReturn_0() throws ParseException {
        int result = findClosestDate.invoke ( matches , format.parse ( "2018-12-1" ) );

        assertEquals(0 , result);

    }

    @Test public void with1_1_2019_sholudReturn_4() throws ParseException {
        int result = findClosestDate.invoke ( matches , format.parse ( "2019-01-01" ) );

        assertEquals(4 , result);

    }


}