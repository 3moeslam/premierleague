package main.sparrow.com.premierleague.presentation.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import junit.framework.TestCase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import main.sparrow.com.premierleague.R;
import main.sparrow.com.premierleague.entities.matches.AwayTeam;
import main.sparrow.com.premierleague.entities.matches.FullTime;
import main.sparrow.com.premierleague.entities.matches.HomeTeam;
import main.sparrow.com.premierleague.entities.matches.MatchesItem;
import main.sparrow.com.premierleague.entities.matches.Score;
import main.sparrow.com.premierleague.presentation.AppTest;
import io.reactivex.observers.TestObserver;
import io.reactivex.subjects.PublishSubject;

@RunWith( RobolectricTestRunner.class )
@Config( application = AppTest.class )
public class MatchesAdapterTest {
    @Mock SharedPreferences sharedPreferences;

    private List <MatchesItem> list = new ArrayList <> ();
    private Context context;
    private RecyclerView recyclerView;
    private PublishSubject <MatchesItem> favoriteClickChannel = PublishSubject.create ();

    @Before public void setUp() {
        context = RuntimeEnvironment.application.getApplicationContext ();
        initializeMatchesList ();
        initializeRecycler ();
    }

    @Test public void numOfItems_should_asPairsList() {
        Assert.assertEquals ( list.size () , recyclerView.getAdapter ().getItemCount () );
    }


    @Test public void firstItemText_should_asFirstInList() {
        View firstItem = recyclerView.getChildAt ( 0 );
        TextView homeTeamName = firstItem.findViewById ( R.id.homeTeamName );
        TextView awayTeamName = firstItem.findViewById ( R.id.awayTeamName );

        TestCase.assertEquals ( "Chelsea" , awayTeamName.getText () );
        TestCase.assertEquals ( "Arsenal" , homeTeamName.getText () );
    }


    @Test public void secondItemText_should_asSecondInList() {
        View secondItem = recyclerView.getChildAt ( 1 );
        TextView homeTeamName = secondItem.findViewById ( R.id.homeTeamName );
        TextView awayTeamName = secondItem.findViewById ( R.id.awayTeamName );

        TestCase.assertEquals ( "Manchester United" , awayTeamName.getText () );
        TestCase.assertEquals ( "Manchester City" , homeTeamName.getText () );
    }

    @Test public void thirdItemText_should_asThirdInList() {
        View thirdItem = recyclerView.getChildAt ( 2 );
        TextView homeTeamName = thirdItem.findViewById ( R.id.homeTeamName );
        TextView awayTeamName = thirdItem.findViewById ( R.id.awayTeamName );

        TestCase.assertEquals ( "Southampton" , awayTeamName.getText () );
        TestCase.assertEquals ( "Liverpool" , homeTeamName.getText () );
    }

    @Test public void whenFINISHED_should_showResult() {
        View firstItem = recyclerView.getChildAt ( 0 );
        TextView statusText = firstItem.findViewById ( R.id.statusText );

        TestCase.assertEquals ( "0 - 1" , statusText.getText () );
    }

    @Test public void whenONGOING_should_showTime() {
        View firstItem = recyclerView.getChildAt ( 2 );
        TextView statusText = firstItem.findViewById ( R.id.statusText );

        TestCase.assertEquals ( "15:00" , statusText.getText () );
    }

    @Test public void whenClickFav_should_emmitItem() {
        View firstItem = recyclerView.getChildAt ( 2 );
        ImageButton favoriteButton = firstItem.findViewById ( R.id.favoriteIcon );
        TestObserver <MatchesItem> observer = favoriteClickChannel.test ();

        favoriteButton.callOnClick ();
        observer.assertValue ( list.get ( 2 ) );
    }

    private void initializeRecycler() {
        recyclerView = new RecyclerView ( context );
        MatchesAdapter adapter = new MatchesAdapter ( list , sharedPreferences , favoriteClickChannel );
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager ( context );
        recyclerView.setLayoutManager ( layoutManager );
        recyclerView.setAdapter ( adapter );
        recyclerView.measure ( 0 , 0 );
        recyclerView.layout ( 0 , 0 , 100 , 1000 );
    }

    private void initializeMatchesList() {
        MatchesItem matchesItem = new MatchesItem ( "" ,
                new Score ( "" , "" , null , null , new FullTime ( 1 , 0 ) , null ) ,
                "" ,
                0 ,
                new AwayTeam ( "Chelsea" , 0 ) ,
                null ,
                new HomeTeam ( "Arsenal" , 1 ) ,
                0 ,
                "2018-12-15T15:00:00Z" ,
                null ,
                "FINISHED" ,
                null ,
                false
        );
        MatchesItem matchesItem1 = new MatchesItem ( "" ,
                new Score ( "" , "" , null , null , new FullTime ( 2 , 0 ) , null ) ,
                "" ,
                0 ,
                new AwayTeam ( "Manchester United" , 0 ) ,
                null ,
                new HomeTeam ( "Manchester City" , 1 ) ,
                0 ,
                "2018-12-15T15:00:00Z" ,
                null ,
                "FINISHED" ,
                null ,
                false
        );
        MatchesItem matchesItem2 = new MatchesItem ( "" ,
                new Score ( "" , "" , null , null , new FullTime ( 1 , 1 ) , null ) ,
                "" ,
                0 ,
                new AwayTeam ( "Southampton" , 0 ) ,
                null ,
                new HomeTeam ( "Liverpool" , 1 ) ,
                0 ,
                "2018-12-15T15:00:00Z" ,
                null ,
                "ONGOING" ,
                null ,
                false
        );

        list.add ( matchesItem );
        list.add ( matchesItem1 );
        list.add ( matchesItem2 );
    }
}