package main.sparrow.com.premierleague.presentation.favorite;

import android.view.View;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import main.sparrow.com.premierleague.entities.matches.AwayTeam;
import main.sparrow.com.premierleague.entities.matches.FullTime;
import main.sparrow.com.premierleague.entities.matches.HomeTeam;
import main.sparrow.com.premierleague.entities.matches.MatchesItem;
import main.sparrow.com.premierleague.entities.matches.Score;
import main.sparrow.com.premierleague.presentation.AppTest;
import main.sparrow.com.premierleague.presentation.common.DataViewModel;
import io.reactivex.subjects.PublishSubject;

import static main.sparrow.com.premierleague.SupportFragmentController.setupFragment;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith( RobolectricTestRunner.class )
@Config( application = AppTest.class )
public class FavoriteFragmentTest {
    //To be able to whenStart_should_showBottomView livedata
    @Rule public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule ();

    @Mock private DataViewModel viewModel;

    private PublishSubject <MatchesItem> favoriteClickChannel = PublishSubject.create ();

    private MutableLiveData <List <MatchesItem>> matches = new MutableLiveData <> ();
    private List <MatchesItem> list = new ArrayList <> ();
    private FavoriteFragment fragment;

    @Before public void setUp() {
        MockitoAnnotations.initMocks ( this );
        when ( viewModel.getFavorite () ).thenReturn ( matches );
        setupList ();
        fragment = setupFragment ( new FavoriteFragment ( viewModel , favoriteClickChannel ) );
    }

    @Test public void with3ItemsList_should_recyclerDisplay3Items() {
        matches.postValue ( list );
        RecyclerView recyclerView = fragment.favoriteRecycler;
        TextView errorText = fragment.errorText;

        assertEquals ( View.GONE , errorText.getVisibility () );
        assertEquals ( list.size () , recyclerView.getAdapter ().getItemCount () );
    }

    @Test public void withEmptyItemsList_should_displayErrorText() {
        matches.postValue ( new ArrayList <> () );
        RecyclerView recyclerView = fragment.favoriteRecycler;
        TextView errorText = fragment.errorText;

        assertEquals ( View.VISIBLE , errorText.getVisibility () );
        assertEquals ( View.GONE , recyclerView.getVisibility () );
    }

    @Test public void withItemOnFavoriteChannel_should_invokeHandleFavoriteInViewModel() {
        favoriteClickChannel.onNext ( list.get ( 0 ) );

        verify ( viewModel ).handleFavoriteClick ( list.get ( 0 ) );
    }

    private void setupList() {
        MatchesItem matchesItem0 = new MatchesItem ( "" ,
                new Score ( "" , "" , null , null , new FullTime ( 1 , 0 ) , null ) ,
                "" ,
                0 ,
                new AwayTeam ( "Chelsia" , 0 ) ,
                null ,
                new HomeTeam ( "Arsenal" , 1 ) ,
                0 ,
                "2018-12-15T15:00:00Z" ,
                null ,
                "FINISHED" ,
                null ,
                false
        );
        MatchesItem matchesItem1 = new MatchesItem ( "" ,
                new Score ( "" , "" , null , null , new FullTime ( 1 , 0 ) , null ) ,
                "" ,
                0 ,
                new AwayTeam ( "Chelsia" , 0 ) ,
                null ,
                new HomeTeam ( "Arsenal" , 1 ) ,
                0 ,
                "2018-12-15T15:00:00Z" ,
                null ,
                "FINISHED" ,
                null ,
                false
        );
        MatchesItem matchesItem2 = new MatchesItem ( "" ,
                new Score ( "" , "" , null , null , new FullTime ( 1 , 0 ) , null ) ,
                "" ,
                0 ,
                new AwayTeam ( "Chelsia" , 0 ) ,
                null ,
                new HomeTeam ( "Arsenal" , 1 ) ,
                0 ,
                "2018-12-15T15:00:00Z" ,
                null ,
                "FINISHED" ,
                null ,
                false
        );

        list.add ( matchesItem0 );
        list.add ( matchesItem1 );
        list.add ( matchesItem2 );
    }

}