package main.sparrow.com.premierleague.domain.database_converters;

import org.junit.Test;

import main.sparrow.com.premierleague.entities.matches.HomeTeam;

import static org.junit.Assert.assertEquals;

public class HomeTeamConverterTest {

    private HomeTeamConverter converter = new HomeTeamConverter ();

    private String jsonAwayTeam = "{\"name\":\"Arsenal\",\"id\":1}";
    private HomeTeam awayTeam = new HomeTeam ( "Arsenal" , 1 );

    @Test public void withJson_ShouldReturnHomeTeam() {
        HomeTeam returnValue = converter.fromStringToHomeTeam ( jsonAwayTeam );

        assertEquals ( awayTeam.getName () , returnValue.getName () );
        assertEquals ( awayTeam.getId () , returnValue.getId () );

    }

    @Test public void withHomeTeam_ShouldReturnJson() {
        String returnValue = converter.fromHomeTeamToString ( awayTeam );

        assertEquals ( jsonAwayTeam , returnValue );
    }
}