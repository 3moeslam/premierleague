package main.sparrow.com.premierleague;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URL;

public class ResourceHelper {

    public String getJson( String path ) throws FileNotFoundException {
        URL uri = this.getClass ().getClassLoader ().getResource ( path );
        FileReader fileReader = new FileReader ( uri.getPath () );
        BufferedReader bufferReader = new BufferedReader ( fileReader );
        StringBuilder stringBuilder = new StringBuilder ();
        bufferReader.lines ()
                .forEach ( stringBuilder::append );
        return stringBuilder.toString ();
    }

}
