package main.sparrow.com.premierleague.presentation;

import android.view.MenuItem;
import android.view.View;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import androidx.navigation.NavController;
import main.sparrow.com.premierleague.R;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith( RobolectricTestRunner.class )
@Config( application = AppTest.AppTestWithKoin.class )
public class MainActivityTest {

    private MainActivity activity;
    @Mock NavController navController;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks ( this );

        activity = Robolectric.setupActivity ( MainActivity.class );

        activity.setNavController ( navController );
    }

    @Test public void clickOnHome_should_invokeNavigate() {
        MenuItem home = mock ( MenuItem.class );
        when ( home.getItemId () ).thenReturn ( R.id.action_home );

        activity.onBottomNavigationSelected ( home );

        verify ( navController ).navigate ( R.id.openHomeFragment );
    }

    @Test public void clickOnFavorite_should_invokeNavigate() {
        MenuItem home = mock ( MenuItem.class );
        when ( home.getItemId () ).thenReturn ( R.id.action_favourite );

        activity.onBottomNavigationSelected ( home );

        verify ( navController ).navigate ( R.id.openFavorite );
    }


    @Test public void showBottomNavigation_should_makeViewVisible() {
        assertEquals ( View.GONE , activity.bottomNavigationView.getVisibility () );

        activity.showBottomView ();

        assertEquals ( View.VISIBLE , activity.bottomNavigationView.getVisibility () );
    }

    @After public void clean(){
        AppTest.AppTestWithKoin app = ( AppTest.AppTestWithKoin ) activity.getApplication ();
        app.stopKoin ();
    }
}