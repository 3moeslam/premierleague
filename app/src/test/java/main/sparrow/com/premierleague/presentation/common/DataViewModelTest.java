package main.sparrow.com.premierleague.presentation.common;

import android.content.SharedPreferences;

import com.google.common.util.concurrent.MoreExecutors;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.TestScheduler;
import kotlin.Pair;
import main.sparrow.com.premierleague.domain.database_gateway.Dao;
import main.sparrow.com.premierleague.domain.network_gateway.PremierLeagueApi;
import main.sparrow.com.premierleague.entities.matches.AwayTeam;
import main.sparrow.com.premierleague.entities.matches.FullTime;
import main.sparrow.com.premierleague.entities.matches.HomeTeam;
import main.sparrow.com.premierleague.entities.matches.MatchesItem;
import main.sparrow.com.premierleague.entities.matches.Score;
import main.sparrow.com.premierleague.entities.teams.TeamsItem;

import static junit.framework.TestCase.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DataViewModelTest {
    //To be able to whenStart_should_showBottomView livedata
    @Rule public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule ();


    @Mock private PremierLeagueApi api;
    @Mock private Dao dao;
    @Mock private Function <PremierLeagueApi, Observable <Pair <String, Iterable <MatchesItem>>>> fixtureLoader;
    @Mock private Function <PremierLeagueApi, Observable <TeamsItem>> teamsLoader;
    @Mock private SharedPreferences sharedPreferences;
    @Mock private SharedPreferences.Editor editor;
    private Executor executor = MoreExecutors.directExecutor ();

    private TestScheduler scheduler = new TestScheduler ();

    private DataViewModel viewModel;
    private List <Pair <String, Iterable <MatchesItem>>> pairs = new ArrayList <> ();
    private List <TeamsItem> teams = new ArrayList <> ();
    private MatchesItem matchesItem;
    private Observable <Pair <String, Iterable <MatchesItem>>> fixtureObservable = Observable.fromIterable ( pairs );
    private Observable <TeamsItem> teamsObservable = Observable.fromIterable ( teams );

    @Before public void setUp() {
        MockitoAnnotations.initMocks ( this );
        initializePairs ();
        initilizeTeams ();
        initilizeMatchItem ();
    }


    @Test public void whenMatchesLoaded_should_postListAndPostLoadingStatus() throws Exception {
        when ( fixtureLoader.apply ( api ) ).thenReturn ( fixtureObservable );
        when ( teamsLoader.apply ( api ) ).thenReturn ( teamsObservable );
        when ( sharedPreferences.edit () ).thenReturn ( editor );

        initializeViewModel ();
        scheduler.triggerActions ();

        assertNotNull ( viewModel.list.getValue () );
        assertNotNull ( viewModel.loadingIndecator.getValue () );
    }


    @Test public void whenTeamsLoadedAndNoCache_should_CacheIt() throws Exception {
        when ( fixtureLoader.apply ( api ) ).thenReturn ( fixtureObservable );
        when ( teamsLoader.apply ( api ) ).thenReturn ( teamsObservable );
        when ( editor.putString ( "Arsenal" , "" ) ).thenReturn ( editor );
        when ( sharedPreferences.getBoolean ( "isLogosCashed" , false ) ).thenReturn ( false );
        when ( editor.putBoolean ( "isLogosCashed" , true ) ).thenReturn ( editor );
        when ( sharedPreferences.edit () ).thenReturn ( editor );

        initializeViewModel ();
        scheduler.triggerActions ();

        verify ( editor , times ( 5 ) ).putString ( "Arsenal" , "" );
    }


    @Test public void whenNoCache_should_NotCallPutString() throws Exception {
        when ( fixtureLoader.apply ( api ) ).thenReturn ( fixtureObservable );
        when ( teamsLoader.apply ( api ) ).thenReturn ( teamsObservable );
        when ( editor.putString ( "Arsenal" , "" ) ).thenReturn ( editor );
        when ( sharedPreferences.getBoolean ( "isLogosCashed" , false ) ).thenReturn ( true );
        when ( editor.putBoolean ( "isLogosCashed" , true ) ).thenReturn ( editor );
        when ( sharedPreferences.edit () ).thenReturn ( editor );

        initializeViewModel ();
        scheduler.triggerActions ();

        verify ( editor , times ( 0 ) ).putString ( "Arsenal" , "" );
    }

    @Test public void whenHandleFavoriteAndItemExist_should_invokeRemoveFroFavorite() throws Exception {
        when ( fixtureLoader.apply ( api ) ).thenReturn ( fixtureObservable );
        when ( teamsLoader.apply ( api ) ).thenReturn ( teamsObservable );
        when ( editor.putString ( "Arsenal" , "" ) ).thenReturn ( editor );
        when ( sharedPreferences.getBoolean ( "isLogosCashed" , false ) ).thenReturn ( true );
        when ( editor.putBoolean ( "isLogosCashed" , true ) ).thenReturn ( editor );
        when ( sharedPreferences.edit () ).thenReturn ( editor );
        when ( dao.getFavouriteItem ( matchesItem.getId () ) ).thenReturn ( matchesItem );

        initializeViewModel ();
        TestObserver <String> observer = viewModel.favoriteMessage.test ();
        viewModel.handleFavoriteClick ( matchesItem );
        verify ( dao ).removeFromFavourite ( matchesItem );
        observer.assertValue ( "Removed From Favorite List" );
    }

    @Test public void whenHandleFavoriteAndItemNotExist_should_invokeAddFroFavorite() throws Exception {
        when ( fixtureLoader.apply ( api ) ).thenReturn ( fixtureObservable );
        when ( teamsLoader.apply ( api ) ).thenReturn ( teamsObservable );
        when ( editor.putString ( "Arsenal" , "" ) ).thenReturn ( editor );
        when ( sharedPreferences.getBoolean ( "isLogosCashed" , false ) ).thenReturn ( true );
        when ( editor.putBoolean ( "isLogosCashed" , true ) ).thenReturn ( editor );
        when ( sharedPreferences.edit () ).thenReturn ( editor );
        when ( dao.getFavouriteItem ( matchesItem.getId () ) ).thenReturn ( null );

        initializeViewModel ();
        TestObserver <String> observer = viewModel.favoriteMessage.test ();
        viewModel.handleFavoriteClick ( matchesItem );
        verify ( dao ).addToFavourite ( matchesItem );
        observer.assertValue (  "Added To Favorite List"  );

    }

    @Test public void whenError_should_emmitOnErrorSubject() throws Exception {
        when ( fixtureLoader.apply ( api ) ).thenReturn ( Observable.error ( new Throwable () ) );
        when ( teamsLoader.apply ( api ) ).thenReturn ( teamsObservable );
        when ( editor.putString ( "Arsenal" , "" ) ).thenReturn ( editor );
        when ( sharedPreferences.getBoolean ( "isLogosCashed" , false ) ).thenReturn ( true );
        when ( editor.putBoolean ( "isLogosCashed" , true ) ).thenReturn ( editor );
        when ( sharedPreferences.edit () ).thenReturn ( editor );
        when ( dao.getFavouriteItem ( matchesItem.getId () ) ).thenReturn ( null );

        initializeViewModel ();
        TestObserver <String> subject = viewModel.networkErrorSubject.test ();
        scheduler.triggerActions ();

        subject.assertValue ( "Sorry There are an error when we load data. please try again" );
    }

    private void initializeViewModel() {
        viewModel = new DataViewModel (
                api ,
                dao ,
                fixtureLoader ,
                teamsLoader ,
                sharedPreferences ,
                scheduler ,
                executor
        );
    }


    private void initializePairs() {
        Pair <String, Iterable <MatchesItem>> pair1 = new Pair <> ( "2018-12-15" , new ArrayList <> () );
        Pair <String, Iterable <MatchesItem>> pair2 = new Pair <> ( "2018-12-17" , new ArrayList <> () );
        Pair <String, Iterable <MatchesItem>> pair3 = new Pair <> ( "2018-12-18" , new ArrayList <> () );
        Pair <String, Iterable <MatchesItem>> pair4 = new Pair <> ( "2018-12-19" , new ArrayList <> () );
        Pair <String, Iterable <MatchesItem>> pair5 = new Pair <> ( "2018-12-25" , new ArrayList <> () );

        pairs.add ( pair1 );
        pairs.add ( pair2 );
        pairs.add ( pair3 );
        pairs.add ( pair4 );
        pairs.add ( pair5 );
    }

    private void initilizeTeams() {
        TeamsItem teamsItem0 = new TeamsItem ( 0 , "Arsenal" , "" );
        TeamsItem teamsItem1 = new TeamsItem ( 1 , "Arsenal" , "" );
        TeamsItem teamsItem2 = new TeamsItem ( 2 , "Arsenal" , "" );
        TeamsItem teamsItem3 = new TeamsItem ( 3 , "Arsenal" , "" );
        TeamsItem teamsItem4 = new TeamsItem ( 4 , "Arsenal" , "" );

        teams.add ( teamsItem0 );
        teams.add ( teamsItem1 );
        teams.add ( teamsItem2 );
        teams.add ( teamsItem3 );
        teams.add ( teamsItem4 );
    }

    private void initilizeMatchItem() {
        matchesItem = new MatchesItem ( "" ,
                new Score ( "" , "" , null , null , new FullTime ( 1 , 0 ) , null ) ,
                "" ,
                0 ,
                new AwayTeam ( "Chelsia" , 0 ) ,
                null ,
                new HomeTeam ( "Arsenal" , 1 ) ,
                0 ,
                "2018-12-15T15:00:00Z" ,
                null ,
                "FINISHED" ,
                null ,
                false
        );
    }
}