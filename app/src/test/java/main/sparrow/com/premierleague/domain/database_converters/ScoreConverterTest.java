package main.sparrow.com.premierleague.domain.database_converters;

import org.junit.Test;

import main.sparrow.com.premierleague.entities.matches.FullTime;
import main.sparrow.com.premierleague.entities.matches.HalfTime;
import main.sparrow.com.premierleague.entities.matches.Score;

import static org.junit.Assert.assertEquals;

public class ScoreConverterTest {

    private ScoreConverter converter = new ScoreConverter ();
    private String scoreJson = "{\"duration\":\"REGULAR\",\"halfTime\":{\"awayTeam\":0,\"homeTeam\":0},\"fullTime\":{\"awayTeam\":0,\"homeTeam\":0}}";
    private Score score = new Score ( "REGULAR" , null , null , new HalfTime ( 0 , 0 ) , new FullTime ( 0 , 0 ) , null );


    @Test public void withJson_ShouldReturnScore() {
        Score returnValue = converter.fromStringToScore ( scoreJson );

        assertEquals ( score.getDuration () , returnValue.getDuration () );
        assertEquals ( score.getFullTime ().getAwayTeam () , returnValue.getFullTime ().getAwayTeam () );
        assertEquals ( score.getFullTime ().getHomeTeam () , returnValue.getFullTime ().getHomeTeam () );
        assertEquals ( score.getHalfTime ().getAwayTeam () , returnValue.getHalfTime ().getAwayTeam () );
        assertEquals ( score.getHalfTime ().getHomeTeam () , returnValue.getHalfTime ().getHomeTeam () );

    }

    @Test public void withHomeTeam_ShouldReturnJson() {
        String returnValue = converter.fromScoreToString ( score );
        System.out.println ( returnValue );
        assertEquals ( scoreJson , returnValue );
    }

}