package main.sparrow.com.premierleague.di

import android.app.Application
import android.content.SharedPreferences
import main.sparrow.com.premierleague.domain.FindClosestDate
import main.sparrow.com.premierleague.domain.FixtureLoader
import main.sparrow.com.premierleague.domain.TeamsLoader
import main.sparrow.com.premierleague.domain.database_gateway.Dao
import main.sparrow.com.premierleague.domain.network_gateway.PremierLeagueApi
import main.sparrow.com.premierleague.entities.matches.MatchesItem
import main.sparrow.com.premierleague.entities.teams.TeamsItem
import main.sparrow.com.premierleague.presentation.common.DataViewModel
import io.reactivex.Observable
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.startKoin
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import org.koin.standalone.StandAloneContext
import org.mockito.Mockito.mock
import java.util.concurrent.Executors


val testAppModule = module {
	single<PremierLeagueApi> { mock(PremierLeagueApi::class.java) }
	single<Dao> { mock(Dao::class.java) }

	factory<Function<PremierLeagueApi, Observable<Pair<String, Iterable<MatchesItem>>>>>("FixtureLoader") { mock(FixtureLoader::class.java) }
	factory<Function<PremierLeagueApi, Observable<TeamsItem>>>("TeamLoader") { mock(TeamsLoader::class.java) }

	factory<Any>("findClosestDate") { mock(FindClosestDate::class.java) }

	factory { Schedulers.io() }
	factory { Executors.newSingleThreadExecutor() }

	factory { mock(SharedPreferences::class.java) }

	viewModel { mock(DataViewModel::class.java) }
}

open class KAppTest : Application() {

	fun startKoin() {
		startKoin(this, listOf(appModule))
	}

	fun stopKoin() {
		StandAloneContext.stopKoin()
	}
}