package main.sparrow.com.premierleague.presentation.home;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import main.sparrow.com.premierleague.R;
import main.sparrow.com.premierleague.entities.matches.AwayTeam;
import main.sparrow.com.premierleague.entities.matches.FullTime;
import main.sparrow.com.premierleague.entities.matches.HomeTeam;
import main.sparrow.com.premierleague.entities.matches.MatchesItem;
import main.sparrow.com.premierleague.entities.matches.Score;
import main.sparrow.com.premierleague.presentation.AppTest;
import io.reactivex.subjects.PublishSubject;
import kotlin.Pair;

import static org.junit.Assert.assertEquals;

@RunWith( RobolectricTestRunner.class )
@Config( application = AppTest.class )
public class FixtureAdapterTest {

    private Context context;

    private RecyclerView recyclerView;
    private List <Pair <String, List <MatchesItem>>> pairs = new ArrayList <> ();
    private PublishSubject <MatchesItem> favoriteClickChannel = PublishSubject.create ();


    @Before public void setUp() {
        context = RuntimeEnvironment.application.getApplicationContext ();

        initializePairs ();

        initializeRecycler ();
    }

    @Test public void numOfItems_should_asPairsList() {
        assertEquals ( pairs.size () , recyclerView.getAdapter ().getItemCount () );
    }

    @Test public void firstItemText_should_asFirstInPairWithSpaces(){
        View firstItem = recyclerView.getChildAt ( 0 );
        TextView dateText = firstItem.findViewById ( R.id.dayDate );

        TestCase.assertEquals ( "2018 - 12 - 15" , dateText.getText () );
    }


    @Test public void lastItemText_should_asLastInPairWithSpaces(){
        View firstItem = recyclerView.getChildAt ( recyclerView.getAdapter ().getItemCount () -1 );
        TextView dateText = firstItem.findViewById ( R.id.dayDate );

        TestCase.assertEquals ( "2018 - 12 - 25" , dateText.getText () );
    }


    private void initializeRecycler() {
        recyclerView = new RecyclerView ( context );
        FixtureAdapter adapter = new FixtureAdapter ( pairs , favoriteClickChannel );
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager ( context );
        recyclerView.setLayoutManager ( layoutManager );
        recyclerView.setAdapter ( adapter );
        recyclerView.measure ( 0 , 0 );
        recyclerView.layout ( 0 , 0 , 100 , 1000 );
    }


    private void initializePairs() {
        MatchesItem matchesItem = new MatchesItem ( "" ,
                new Score ( "" , "" , null , null , new FullTime ( 1 , 0 ) , null ) ,
                "" ,
                0 ,
                new AwayTeam ( "Chelsia" , 0 ) ,
                null ,
                new HomeTeam ( "Arsenal" , 1 ) ,
                0 ,
                "2018-12-15T15:00:00Z" ,
                null ,
                "FINISHED" ,
                null ,
                false
        );

        Pair <String, List <MatchesItem>> pair1 = new Pair <> ( "2018-12-15" , new ArrayList <> () );
        Pair <String, List <MatchesItem>> pair2 = new Pair <> ( "2018-12-17" , new ArrayList <> () );
        Pair <String, List <MatchesItem>> pair3 = new Pair <> ( "2018-12-18" , new ArrayList <> () );
        Pair <String, List <MatchesItem>> pair4 = new Pair <> ( "2018-12-19" , new ArrayList <> () );
        Pair <String, List <MatchesItem>> pair5 = new Pair <> ( "2018-12-25" , new ArrayList <> () );

        pairs.add ( pair1 );
        pairs.add ( pair2 );
        pairs.add ( pair3 );
        pairs.add ( pair4 );
        pairs.add ( pair5 );
    }

}