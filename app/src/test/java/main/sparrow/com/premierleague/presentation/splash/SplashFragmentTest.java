package main.sparrow.com.premierleague.presentation.splash;

import android.content.DialogInterface;
import android.view.View;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;
import androidx.navigation.NavController;
import main.sparrow.com.premierleague.R;
import main.sparrow.com.premierleague.presentation.AppTest;
import main.sparrow.com.premierleague.presentation.common.DataViewModel;
import io.reactivex.subjects.PublishSubject;

import static main.sparrow.com.premierleague.SupportFragmentController.setupFragment;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith( RobolectricTestRunner.class )
@Config( application = AppTest.class )
public class SplashFragmentTest {
    //To be able to test livedata
    @Rule public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule ();


    @Mock private DataViewModel dataViewModel;
    @Mock private NavController navController;
    private SplashFragment fragment;
    private MutableLiveData <Boolean> liveData;
    private PublishSubject<String> networkErrorSubject = PublishSubject.create ();
    @Before public void setUp() {
        MockitoAnnotations.initMocks ( this );

        liveData = new MutableLiveData <> ();

        dataViewModel.loadingIndecator = liveData;
        dataViewModel.networkErrorSubject = networkErrorSubject;

        fragment = setupFragment ( new SplashFragment ( dataViewModel , navController ) );
    }

    @Test public void whenFinishAnim_should_viewsVisible() throws InterruptedException {
        //wait anim time
        Thread.sleep ( 700 );

        assertEquals ( View.VISIBLE , fragment.loadingText.getVisibility () );
        assertEquals ( View.VISIBLE , fragment.logoImage.getVisibility () );
        assertEquals ( View.VISIBLE , fragment.progressBar.getVisibility () );
    }

    @Test public void whenFinishLoading_should_invokeNavigation() throws InterruptedException {
        liveData.postValue ( true );
        //wait anim time
        Thread.sleep ( 700 );

        //3 Animations done means 3 navigate invoke
        verify ( navController , times ( 3 ) ).navigate ( R.id.openHomeFragment );
    }

    @Test public void whenErrorSubjectEmmit_should_dialogNotNull() throws InterruptedException {
        //wait anim time
        Thread.sleep ( 700 );
        networkErrorSubject.onNext ( "Error" );
        assertNotNull ( fragment.dialog );
    }

    @Test public void whenErrorSubjectEmmitAndTryAgainPress_should_viewModelReloadShouldInvoked() throws InterruptedException {
        //wait anim time
        Thread.sleep ( 700 );
        networkErrorSubject.onNext ( "Error" );
        assertNotNull ( fragment.dialog );
        fragment.dialog.getButton ( DialogInterface.BUTTON_POSITIVE ).callOnClick ();
        verify ( dataViewModel ).reloadData ();
    }

}