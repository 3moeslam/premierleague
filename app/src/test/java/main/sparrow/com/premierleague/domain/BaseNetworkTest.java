package main.sparrow.com.premierleague.domain;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.FileNotFoundException;
import java.io.IOException;

import main.sparrow.com.premierleague.ResourceHelper;
import main.sparrow.com.premierleague.domain.network_gateway.PremierLeagueApi;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

abstract class BaseNetworkTest {

    private static final String API_KEY = "4e4bd0c9a8584379aca7f3c630ae2b60";


    MockWebServer mockWebServer;
    PremierLeagueApi premierLeagueApi;


    void startServer() throws IOException {
        mockWebServer = new MockWebServer ();
        mockWebServer.start ();
    }

    Retrofit buildRetrofit() {
        return new Retrofit.Builder ()
                .baseUrl ( getBaseUrl () )
                .addConverterFactory ( GsonConverterFactory.create () )
                .addCallAdapterFactory ( RxJava2CallAdapterFactory.create () )
                .client ( buildOkHttpClient () )
                .build ();
    }

    private String getBaseUrl() {
        return "http://" + mockWebServer.getHostName () + ":" + mockWebServer.getPort ();
    }


    private OkHttpClient buildOkHttpClient() {
        return new OkHttpClient.Builder ()
                .addInterceptor ( provideHeaderInterceptor () )
                .build ();

    }

    String getJson( String path ) throws FileNotFoundException {
        return new ResourceHelper ().getJson ( path );
    }

    private Interceptor provideHeaderInterceptor() {
        return chain -> {
            Request request = chain.request ();
            Request newRequest = request.newBuilder ()
                    .removeHeader ( "X-Auth-Token" )
                    .addHeader ( "X-Auth-Token" , API_KEY )
                    .addHeader ( "User-Agent" , "android-client" )
                    .build ();

            return chain.proceed ( newRequest );
        };
    }
}
