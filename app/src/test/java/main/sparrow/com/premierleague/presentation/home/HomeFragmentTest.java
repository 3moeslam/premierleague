package main.sparrow.com.premierleague.presentation.home;

import android.view.View;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.subjects.PublishSubject;
import main.sparrow.com.premierleague.R;
import main.sparrow.com.premierleague.domain.FindClosestDate;
import main.sparrow.com.premierleague.entities.matches.AwayTeam;
import main.sparrow.com.premierleague.entities.matches.FullTime;
import main.sparrow.com.premierleague.entities.matches.HomeTeam;
import main.sparrow.com.premierleague.entities.matches.MatchesItem;
import main.sparrow.com.premierleague.entities.matches.Score;
import main.sparrow.com.premierleague.presentation.AppTest;
import main.sparrow.com.premierleague.presentation.common.DataViewModel;
import kotlin.Pair;

import static main.sparrow.com.premierleague.SupportFragmentController.setupFragment;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.verify;

@RunWith( RobolectricTestRunner.class )
@Config( application = AppTest.class )
public class HomeFragmentTest {
    //To be able to whenStart_should_showBottomView livedata
    @Rule public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule ();


    @Mock private DataViewModel viewModel;
    @Mock private FindClosestDate findClosestDate;
    @Mock private Runnable showBottomView;

    private List <Pair <String, List <MatchesItem>>> pairs = new ArrayList <> ();
    private HomeFragment fragment;

    private PublishSubject<String> favoriteMessage = PublishSubject.create ();
    private MutableLiveData <List <Pair <String, List <MatchesItem>>>> list = new MutableLiveData ();

    @Before public void setUp() {
        MockitoAnnotations.initMocks ( this );

        initializePairs ();

        viewModel.list = list;
        viewModel.favoriteMessage = favoriteMessage;
        list.setValue ( pairs );
        fragment = setupFragment ( new HomeFragment ( viewModel , findClosestDate , showBottomView ) );
    }


    @Test public void whenStart_should_showBottomView() {
        verify ( showBottomView ).run ();
    }


    @Test public void firstRecyclerItem_should_showDate() {
        RecyclerView recyclerView = fragment.fixtureRecycler;
        recyclerView.measure ( 0 , 0 );
        recyclerView.layout ( 0 , 0 , 100 , 1000 );
        View firstItem = recyclerView.getChildAt ( 0 );
        TextView dateText = firstItem.findViewById ( R.id.dayDate );

        assertEquals ( "2018 - 12 - 15" , dateText.getText () );
    }

    @Test public void emmitItemInFavorite_should_invokeViewModel_handleFavoriteClick() {
        MatchesItem item = new MatchesItem ();
        fragment.favoriteClickChannel.onNext ( item );
        verify ( viewModel ).handleFavoriteClick ( item );
    }

    @Test public void mustSubscribeToFavoriteMessage() {
        assertTrue ( favoriteMessage.hasObservers () );
    }

    private void initializePairs() {
        MatchesItem matchesItem = new MatchesItem ( "" ,
                new Score ( "" , "" , null , null , new FullTime ( 1 , 0 ) , null ) ,
                "" ,
                0 ,
                new AwayTeam ( "Chelsia" , 0 ) ,
                null ,
                new HomeTeam ( "Arsenal" , 1 ) ,
                0 ,
                "2018-12-15T15:00:00Z" ,
                null ,
                "FINISHED" ,
                null ,
                false
        );

        Pair <String, List <MatchesItem>> pair1 = new Pair <> ( "2018-12-15" , new ArrayList <> () );
        Pair <String, List <MatchesItem>> pair2 = new Pair <> ( "2018-12-17" , new ArrayList <> () );
        Pair <String, List <MatchesItem>> pair3 = new Pair <> ( "2018-12-18" , new ArrayList <> () );
        Pair <String, List <MatchesItem>> pair4 = new Pair <> ( "2018-12-19" , new ArrayList <> () );
        Pair <String, List <MatchesItem>> pair5 = new Pair <> ( "2018-12-25" , new ArrayList <> () );

        pairs.add ( pair1 );
        pairs.add ( pair2 );
        pairs.add ( pair3 );
        pairs.add ( pair4 );
        pairs.add ( pair5 );
    }


}