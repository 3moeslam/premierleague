package main.sparrow.com.premierleague.domain;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import main.sparrow.com.premierleague.domain.network_gateway.PremierLeagueApi;
import main.sparrow.com.premierleague.entities.matches.MatchesItem;
import io.reactivex.observers.TestObserver;
import kotlin.Pair;
import okhttp3.mockwebserver.MockResponse;
import retrofit2.Retrofit;

import static junit.framework.TestCase.assertEquals;

public class FixtureLoaderTest extends BaseNetworkTest {

    private FixtureLoader fixtureLoader = new FixtureLoader ();


    @Before public void setUp() throws IOException {
        startServer ();
        Retrofit retrofit = buildRetrofit ();

        premierLeagueApi = retrofit.create ( PremierLeagueApi.class );
    }

    @Test public void withValidResponse_should_noErrors() throws FileNotFoundException {
        MockResponse mockResponse = new MockResponse ()
                .setResponseCode ( 200 )
                .setBody ( getJson ( "json/matches_response.json" ) );

        mockWebServer.enqueue ( mockResponse );


        TestObserver <Pair <String, Iterable <MatchesItem>>> observer = fixtureLoader.apply ( premierLeagueApi )
                .test ();

        observer.awaitTerminalEvent ( 2 , TimeUnit.SECONDS );
        observer.assertNoErrors ();
    }


    @Test public void withInvalidResponse_assertError() {
        MockResponse mockResponse = new MockResponse ()
                .setResponseCode ( 200 )
                .setBody ( "{}" );

        mockWebServer.enqueue ( mockResponse );

        TestObserver <Pair <String, Iterable <MatchesItem>>> observer = fixtureLoader.apply ( premierLeagueApi )
                .test ();

        observer.awaitTerminalEvent ();
        assertEquals ( 1 , observer.errorCount () );
    }


    @Test public void with404_assertError() {
        MockResponse mockResponse = new MockResponse ()
                .setResponseCode ( 404 );

        mockWebServer.enqueue ( mockResponse );

        TestObserver <Pair <String, Iterable <MatchesItem>>> observer = fixtureLoader.apply ( premierLeagueApi )
                .test ();

        observer.awaitTerminalEvent ();
        assertEquals ( 1 , observer.errorCount () );
    }


    @Test public void withResourseResponse_should_2Pairs() throws FileNotFoundException {
        MockResponse mockResponse = new MockResponse ()
                .setResponseCode ( 200 )
                .setBody ( getJson ( "json/matches_response.json" ) );

        mockWebServer.enqueue ( mockResponse );

        TestObserver <Pair <String, Iterable <MatchesItem>>> observer = fixtureLoader.apply ( premierLeagueApi )
                .test ();

        observer.awaitTerminalEvent ();
        observer.assertNoErrors ();
        observer.assertComplete ();
        observer.assertValueCount ( 2 );
    }

    @After public void tearDown() throws IOException {
        mockWebServer.shutdown ();
    }


}