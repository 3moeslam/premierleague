package main.sparrow.com.premierleague.domain.database_converters;

import org.junit.Test;

import main.sparrow.com.premierleague.entities.matches.AwayTeam;

import static org.junit.Assert.*;

public class AwayTeamConverterTest {

    private AwayTeamConverter converter = new AwayTeamConverter ();
    private String jsonAwayTeam = "{\"name\":\"Arsenal\",\"id\":1}";
    private AwayTeam awayTeam = new AwayTeam ("Arsenal",1);

    @Test public void withJson_ShouldReturnAwayTeam(){
        AwayTeam returnValue = converter.fromStringToHomeTeam ( jsonAwayTeam );

        assertEquals ( awayTeam.getName () , returnValue.getName () );
        assertEquals ( awayTeam.getId () , returnValue.getId () );

    }

    @Test public void withAwayTeam_ShouldReturnJson(){
        String returnValue = converter.fromAwayTeamToString ( awayTeam );

        assertEquals ( jsonAwayTeam , returnValue );
    }

}