package main.sparrow.com.premierleague.presentation;

import main.sparrow.com.premierleague.di.KApp;
import main.sparrow.com.premierleague.di.KAppTest;

/**
 * Application class Helper in Robolectric unit tests
 * Mainly it used to not create koin
 */
public class AppTest extends KAppTest {

    @Override public void onCreate() {
        super.onCreate ();
    }

    public static class AppTestWithKoin extends KApp {
        @Override public void onCreate() {
            super.onCreate ();

            startKoin ();
        }
    }
}