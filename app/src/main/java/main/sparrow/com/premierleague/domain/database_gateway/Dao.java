package main.sparrow.com.premierleague.domain.database_gateway;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import main.sparrow.com.premierleague.entities.matches.MatchesItem;

@androidx.room.Dao
public interface Dao {
    @Insert( onConflict = OnConflictStrategy.REPLACE )
    void addToFavourite( MatchesItem fixturesItem );

    @Delete
    void removeFromFavourite( MatchesItem fixturesItem );

    @Query( "select * from FavouriteMatches" )
    LiveData <List <MatchesItem>> getFavourite();


    @Query( "select * from FavouriteMatches where id=:id" )
    MatchesItem getFavouriteItem(int id);
}
