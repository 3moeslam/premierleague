package main.sparrow.com.premierleague.domain.database_converters;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import androidx.room.TypeConverter;
import main.sparrow.com.premierleague.entities.matches.HomeTeam;


/**
 * Room Type Converter class To Convert form {@link HomeTeam} to {@link String} in Json Format
 * We will use Gson to convert Pojo to be able to store it in table
 * Class methods used by Room
 */
public class HomeTeamConverter {

    /**
     * Convert {@link HomeTeam} to {@link String}
     * @param homeTeam pojo to convert
     * @return json representation
     */
    @TypeConverter
    public String fromHomeTeamToString( HomeTeam homeTeam ) {
        if ( homeTeam == null ) {
            return (null);
        }
        Gson gson = new Gson ();
        Type type = new TypeToken<HomeTeam> () {
        }.getType ();
        return gson.toJson ( homeTeam , type );
    }

    /**
     * Convert {@link String} to {@link HomeTeam}
     * @param homeTeamString json representation
     * @return generated from json
     */
    @TypeConverter
    public HomeTeam fromStringToHomeTeam( String homeTeamString ) {
        if ( homeTeamString == null ) {
            return (null);
        }
        Gson gson = new Gson ();
        Type type = new TypeToken <HomeTeam> () {
        }.getType ();
        return gson.fromJson ( homeTeamString , type );
    }
}
