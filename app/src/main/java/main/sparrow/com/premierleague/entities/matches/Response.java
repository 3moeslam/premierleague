package main.sparrow.com.premierleague.entities.matches;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Response {

    @SerializedName( "count" )
    private int count;

    @SerializedName( "competition" )
    private Competition competition;

    @SerializedName( "filters" )
    private Filters filters;

    @SerializedName( "matches" )
    private List <MatchesItem> matches;

    public int getCount() {
        return count;
    }

    public Competition getCompetition() {
        return competition;
    }

    public Filters getFilters() {
        return filters;
    }

    public List <MatchesItem> getMatches() {
        return matches;
    }

    @Override
    public String toString() {
        return
                "TeamsResponse{" +
                        "count = '" + count + '\'' +
                        ",competition = '" + competition + '\'' +
                        ",filters = '" + filters + '\'' +
                        ",matches = '" + matches + '\'' +
                        "}";
    }
}