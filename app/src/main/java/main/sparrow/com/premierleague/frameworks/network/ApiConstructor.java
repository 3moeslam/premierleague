package main.sparrow.com.premierleague.frameworks.network;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import main.sparrow.com.premierleague.domain.network_gateway.PremierLeagueApi;
import main.sparrow.com.premierleague.entities.interfaces.Supplier;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * ApiConstructor
 * Used to Implement Domain Layer Gateway interface {@link PremierLeagueApi}
 * It take nothing
 * and return Implementation of {@link PremierLeagueApi}
 * Constructor will add all nessesary server connection params like auth headers
 */
public class ApiConstructor implements Supplier <PremierLeagueApi> {

    //Api Key Generated from (https://www.football-data.org/client/register)
    private static final String API_KEY = "4e4bd0c9a8584379aca7f3c630ae2b60";
    //Base Api url (V1 provided in task requirement not include this season)
    private static final String BASE_URL = "https://api.football-data.org/v2/";

    //Main Logic required to implement Gateway interface
    @Override public PremierLeagueApi apply() {
        //Gson factory converter to auto parse json
        Gson gsonFactory = provideGsonFactory ();
        //Http Client include logging and Auth headers
        OkHttpClient httpClient = provideHttpClient ();

        Retrofit builder = new Retrofit.Builder ()
                .baseUrl ( BASE_URL )
                .addConverterFactory ( GsonConverterFactory.create ( gsonFactory ) )
                //RxJava2 To Be able to return responses in Reactive Pattern
                .addCallAdapterFactory ( RxJava2CallAdapterFactory.create () )
                .client ( httpClient )
                .build ();

        //Create Gateway with builder and return it
        return builder.create ( PremierLeagueApi.class );
    }

    /**
     * Create Gson with any configuration and to used directly in retrofit
     * @return {@link Gson} instance
     */
    private Gson provideGsonFactory() {
        return new GsonBuilder ().create ();
    }

    /**
     * Create {@link OkHttpClient} based with logging and auth headers to used directly in retrofit
     * @return {@link OkHttpClient} instance
     */
    private OkHttpClient provideHttpClient() {
        //Create OkHttpClient Builder and return it
        return new OkHttpClient.Builder ()
                //Add Auth Headers Interceptor
                .addInterceptor ( provideHeaderInterceptor () )
                //Add Logging interceptor
                .addInterceptor ( buildLoggingInterceptor() )
                .build ();
    }




    /**
     * Create Implementation for {@link Interceptor} Include all nessesary headers
     * @see <a href="https://github.com/square/okhttp/wiki/Interceptors">Interceptors</a>
     * @return {@link Interceptor}
     */
    private Interceptor provideHeaderInterceptor() {
        return chain -> {
            Request request = chain.request ();
            Request newRequest = request.newBuilder ()
                    .removeHeader ( "X-Auth-Token" )
                    //Add Auth Api Key
                    .addHeader ( "X-Auth-Token" , API_KEY )
                    //Add User agent
                    .addHeader ( "User-Agent" , "android-client" )
                    .build ();

            return chain.proceed ( newRequest );
        };
    }

    /**
     * Create Implementation for Interceptor used to Logging
     * @see <a href="https://github.com/square/okhttp/wiki/Interceptors">Interceptors</a>
     * @return {@link HttpLoggingInterceptor}
     */
    private HttpLoggingInterceptor buildLoggingInterceptor() {
        return new HttpLoggingInterceptor ().setLevel ( HttpLoggingInterceptor.Level.BODY );
    }
}
