package main.sparrow.com.premierleague.entities.matches;


import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.TestOnly;


public class HalfTime {

    @SerializedName( "awayTeam" )
    private int awayTeam;

    @SerializedName( "homeTeam" )
    private int homeTeam;

    @TestOnly
    public HalfTime( int awayTeam , int homeTeam ) {
        this.awayTeam = awayTeam;
        this.homeTeam = homeTeam;
    }

    public int getAwayTeam() {
        return awayTeam;
    }

    public int getHomeTeam() {
        return homeTeam;
    }

    @Override
    public String toString() {
        return
                "HalfTime{" +
                        "awayTeam = '" + awayTeam + '\'' +
                        ",homeTeam = '" + homeTeam + '\'' +
                        "}";
    }
}