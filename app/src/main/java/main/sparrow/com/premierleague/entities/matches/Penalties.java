package main.sparrow.com.premierleague.entities.matches;


import com.google.gson.annotations.SerializedName;


public class Penalties {

    @SerializedName( "awayTeam" )
    private Object awayTeam;

    @SerializedName( "homeTeam" )
    private Object homeTeam;

    public Object getAwayTeam() {
        return awayTeam;
    }

    public Object getHomeTeam() {
        return homeTeam;
    }

    @Override
    public String toString() {
        return
                "Penalties{" +
                        "awayTeam = '" + awayTeam + '\'' +
                        ",homeTeam = '" + homeTeam + '\'' +
                        "}";
    }
}