package main.sparrow.com.premierleague.presentation.common;

import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import main.sparrow.com.premierleague.domain.database_gateway.Dao;
import main.sparrow.com.premierleague.domain.network_gateway.PremierLeagueApi;
import main.sparrow.com.premierleague.entities.matches.MatchesItem;
import main.sparrow.com.premierleague.entities.teams.TeamsItem;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.subjects.PublishSubject;
import kotlin.Pair;
import timber.log.Timber;

/**
 * DataViewModel
 * Main View Model used by all fragments because it know how to load data from server and sqlite
 * Functionalities
 * 1- Load matches data when start
 * 2- Load teams data and cache team logo in shared prefrences to load it fast in adapter
 * 3- Load Favorite item from database
 * 4- Add Favorite item to database
 */
public class DataViewModel extends ViewModel {

    // MutableLiveData latch pair list
    public MutableLiveData <List <Pair <String, List <MatchesItem>>>> list = new MutableLiveData ();
    // MutableLiveData latch loading status
    public MutableLiveData <Boolean> loadingIndecator = new MutableLiveData <> ();
    // PublishSubject to emmit favorite message
    public PublishSubject <String> favoriteMessage = PublishSubject.create ();

    public PublishSubject<String> networkErrorSubject = PublishSubject.create ();
    // list loaded from server
    private List <Pair <String, List <MatchesItem>>> fixtureList = new ArrayList <> ();
    // Network gateway implemetantion
    private PremierLeagueApi api;
    // Database gateway implementation
    private Dao dao;

    // Fixture loading usecase
    private Function <PremierLeagueApi, Observable <Pair <String, Iterable <MatchesItem>>>> fixtureLoader;
    // Team loading use case
    private Function <PremierLeagueApi, Observable <TeamsItem>> teamsLoader;
    // RxSceduler
    private Scheduler scheduler;
    // Shared Prefernces
    private SharedPreferences sharedPreferences;
    // Executor to run block of code in background thread
    private Executor executor;
    //RxJava Disposables
    private CompositeDisposable disposables = new CompositeDisposable ();

    /**
     * Main View Model used by all fragments because it know how to load data from server and sqlite
     *
     * @param api               Network gateway implemetantion
     * @param dao               Database gateway implementation
     * @param fixtureLoader     Fixture loading usecase
     * @param teamsLoader       Team loading use case
     * @param sharedPreferences Shared Prefernces
     * @param scheduler         RxSceduler
     * @param executor          Executor to run block of code in background thread
     */
    public DataViewModel( PremierLeagueApi api ,
                          Dao dao ,
                          Function <PremierLeagueApi, Observable <Pair <String, Iterable <MatchesItem>>>> fixtureLoader ,
                          Function <PremierLeagueApi, Observable <TeamsItem>> teamsLoader ,
                          SharedPreferences sharedPreferences ,
                          Scheduler scheduler ,
                          Executor executor ) {

        // Add injected parameters to class variables
        this.api = api;
        this.dao = dao;
        this.executor = executor;

        this.fixtureLoader = fixtureLoader;
        this.teamsLoader = teamsLoader;
        this.scheduler = scheduler;
        this.sharedPreferences = sharedPreferences;

        try {
            //Load Fixture and teams
            loadFixture ();
            loadAndSaveTeamsLogosIfNeeded ();
        } catch ( Throwable throwable ) {
            throwable.printStackTrace ();
            onNetworkError ( throwable );
        }
    }

    /**
     * Check if teams logos is not cached it will cache it
     *
     * @throws Exception Exception when load teams
     */
    private void loadAndSaveTeamsLogosIfNeeded() throws Exception {
        boolean isLogosCashed = sharedPreferences.getBoolean ( "isLogosCashed" , false );

        if ( !isLogosCashed ) {
            loadAndSavaTeams ();
        }
    }

    /**
     * Invock TeamLoader use case to load teams using given scheduler
     *
     * @throws Exception Exception when load teams
     */
    private void loadAndSavaTeams() throws Exception {
        Disposable disposable = teamsLoader.apply ( api )
                .subscribeOn ( scheduler )
                .observeOn ( scheduler )
                .subscribe ( this::onNewTeam , this::onNetworkError , this::onImagesLoadingFinished );

        disposables.add ( disposable );
    }

    /**
     * Called when finish load teams from server
     */
    private void onImagesLoadingFinished() {
        sharedPreferences.edit ()
                .putBoolean ( "isLogosCashed" , true )
                .apply ();
    }

    /**
     * Called when team loaded from server
     * It must cache it in shared pref
     *
     * @param item loaded team item
     */
    private void onNewTeam( TeamsItem item ) {
        Timber.i ( "%s -> %s" , item.getName () , item.getCrestUrl () );
        sharedPreferences.edit ()
                .putString ( item.getName () , item.getCrestUrl () )
                .apply ();
    }

    /**
     * Load Fixture (Matches) from server
     *
     * @throws Exception Error Happened when call fixtureLoader
     */
    private void loadFixture() throws Exception {
        Disposable d = fixtureLoader.apply ( api )
                .subscribeOn ( scheduler )
                .observeOn ( scheduler )
                .subscribe ( this::onNewData , this::onNetworkError , this::onLoadingFinished );
        disposables.add ( d );
    }

    /**
     * reload Fixture and teams from server
     */
    public void reloadData(){
        try {
            //Load Fixture and teams
            loadFixture ();
            loadAndSaveTeamsLogosIfNeeded ();
        } catch ( Throwable throwable ) {
            throwable.printStackTrace ();
            onNetworkError ( throwable );
        }
    }

    /**
     * Called when new pair of data loaded
     * It must create a new list because given pair are not reusable because it out from groupBy operator
     *
     * @param stringIterablePair pair of date {@link String} and List of {@link MatchesItem}
     */
    private void onNewData( Pair <String, Iterable <MatchesItem>> stringIterablePair ) {
        //create new list
        List <MatchesItem> list = new ArrayList <> ();
        //loop though MatchesItem iterable and add each item to new list
        for ( MatchesItem item : stringIterablePair.getSecond () ) {
            list.add ( item );
        }
        //create new pair
        Pair <String, List <MatchesItem>> pair = new Pair <> ( stringIterablePair.getFirst () , list );
        //add pair to list
        fixtureList.add ( pair );
    }


    /**
     * Called when network error occur
     *
     * @param throwable Instance of error
     */
    private void onNetworkError( Throwable throwable ) {
        Timber.i ( throwable );
        networkErrorSubject.onNext ( "Sorry There are an error when we load data. please try again" );
    }


    /**
     * Called when finish load data from server
     */
    private void onLoadingFinished() {
        //post pairs list
        list.postValue ( fixtureList );
        // update loading status
        loadingIndecator.postValue ( true );
    }

    /**
     * Handle Favorite click
     * if favorite item in database it will removed
     * else it will added
     *
     * @param matchesItem item to handle
     */
    public void handleFavoriteClick( MatchesItem matchesItem ) {
        //run in background thread
        executor.execute ( () -> {
            //get given item from database
            MatchesItem item = dao.getFavouriteItem ( matchesItem.getId () );
            //check if it exist in database
            if ( item == null ) {
                //if not add it to favorite list
                addToFavorite ( matchesItem );
                //post added favorite message
                favoriteMessage.onNext ( "Added To Favorite List" );
            } else {
                //if exist remove it from favorite
                removeFromFavorite ( matchesItem );
                //post removed from favorite message
                favoriteMessage.onNext ( "Removed From Favorite List" );
            }
        } );
    }

    /**
     * Load Favorite List from sqlite database
     *
     * @return LiveData holdes List of Favorite matches
     */
    public LiveData <List <MatchesItem>> getFavorite() {
        return dao.getFavourite ();
    }

    /**
     * Add given item to favorite table
     *
     * @param fixturesItem item to added
     */
    private void addToFavorite( MatchesItem fixturesItem ) {
        executor.execute ( () -> dao.addToFavourite ( fixturesItem ) );
    }

    /**
     * Remove given item from favorite database
     *
     * @param fixturesItem item to removed
     */
    private void removeFromFavorite( MatchesItem fixturesItem ) {
        executor.execute ( () -> dao.removeFromFavourite ( fixturesItem ) );
    }

    @Override protected void onCleared() {
        disposables.clear ();
        super.onCleared ();
    }

}
