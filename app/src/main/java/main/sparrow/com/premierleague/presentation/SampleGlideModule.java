package main.sparrow.com.premierleague.presentation;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Glide Module needed to create GlideApp class
 * to work with glide plugin to work with svg format
 * @see https://github.com/kirich1409/SvgGlidePlugins
 */
@GlideModule
public class SampleGlideModule extends AppGlideModule {
    @Override
    public boolean isManifestParsingEnabled() {
        return false;
    }
}
