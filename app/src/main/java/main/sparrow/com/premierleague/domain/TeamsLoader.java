package main.sparrow.com.premierleague.domain;


import main.sparrow.com.premierleague.domain.network_gateway.PremierLeagueApi;
import main.sparrow.com.premierleague.entities.teams.TeamsItem;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Teams Loader use case
 * This use case used to Load PL Teams
 */
public class TeamsLoader implements Function<PremierLeagueApi, Observable <TeamsItem>> {

    @Override public Observable <TeamsItem> apply( PremierLeagueApi premierLeagueApi ) {
        //get response from server
        return premierLeagueApi.allTeamsInCompetition ()
                //emmit new stream for teams
                .flatMap ( teamsResponse -> Observable.fromIterable ( teamsResponse.getTeams () ) );
    }
}
