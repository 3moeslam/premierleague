package main.sparrow.com.premierleague.domain;



import main.sparrow.com.premierleague.domain.network_gateway.PremierLeagueApi;
import main.sparrow.com.premierleague.entities.matches.MatchesItem;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import kotlin.Pair;

/**
 * Fixture Loader use case
 * This use case apply required business num 3 (The list should be sectioned by day)
 * It Implements Function to reach higher level in abstraction, so it can be replaced with changing class name in di without anything else,
 * also it will be helpful in unit testing by mocking it easily.
 * Input PremierLeagueApi implementation of Network Gateway
 * Return Observable <Pair <String, Iterable <MatchesItem>>>
 */
public class FixtureLoader implements Function <PremierLeagueApi, Observable <Pair <String, Iterable <MatchesItem>>>> {

    @Override public Observable <Pair <String, Iterable <MatchesItem>>> apply( PremierLeagueApi premierLeagueApi ) {

        //Load and parse Response from server
        return premierLeagueApi.getFixture ()
                //emmit new stream from each match
                .flatMap ( response -> Observable.fromIterable ( response.getMatches () ) )
                //group matches by date
                .groupBy ( matchesItem -> matchesItem.getUtcDate ().split ( "T" )[ 0 ] )
                //map observable that emitted from groupby operator to pair first is date and second is list of matches
                .map ( observable -> {
                    String dateTime = observable.getKey ();
                    String date = dateTime.split ( "T" )[ 0 ];
                    Iterable <MatchesItem> list = observable.blockingIterable ();
                    return new Pair <> ( date , list );
                } );
    }

}
