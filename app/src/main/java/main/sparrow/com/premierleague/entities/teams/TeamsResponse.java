package main.sparrow.com.premierleague.entities.teams;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class TeamsResponse {

	@SerializedName("teams")
	private List<TeamsItem> teams;

	@SerializedName("count")
	private int count;

	@SerializedName("season")
	private Season season;

	@SerializedName("competition")
	private Competition competition;

	@SerializedName("filters")
	private Filters filters;

	public List<TeamsItem> getTeams(){
		return teams;
	}

	public int getCount(){
		return count;
	}

	public Season getSeason(){
		return season;
	}

	public Competition getCompetition(){
		return competition;
	}

	public Filters getFilters(){
		return filters;
	}

	@Override
 	public String toString(){
		return 
			"TeamsResponse{" +
			"teams = '" + teams + '\'' + 
			",count = '" + count + '\'' + 
			",season = '" + season + '\'' + 
			",competition = '" + competition + '\'' + 
			",filters = '" + filters + '\'' + 
			"}";
		}
}