package main.sparrow.com.premierleague.entities.fixture;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.room.Entity;
import androidx.room.Ignore;

@Entity( tableName = "FavouriteFixtures" )
public class FixturesItem {

    @SerializedName( "date" )
    private String date;

    @SerializedName( "result" )
    private Result result;

    @SerializedName( "_links" ) @Ignore
    private Links links;

    @SerializedName( "matchday" )
    private int matchday;

    @SerializedName( "odds" ) @Ignore
    private Object odds;

    @SerializedName( "awayTeamName" )
    private String awayTeamName;

    @SerializedName( "homeTeamName" )
    private String homeTeamName;

    @SerializedName( "status" )
    private String status;


    @Expose( serialize = false, deserialize = false )
    private String homeTeamLogo;

    @Expose( serialize = false, deserialize = false )
    private String awayTeamLogo;

    @Expose( serialize = false, deserialize = false )
    private boolean isFavorite;

    public String getDate() {
        return date;
    }

    public Result getResult() {
        return result;
    }

    public Links getLinks() {
        return links;
    }

    public int getMatchday() {
        return matchday;
    }

    public Object getOdds() {
        return odds;
    }

    public String getAwayTeamName() {
        return awayTeamName;
    }

    public String getHomeTeamName() {
        return homeTeamName;
    }

    public String getStatus() {
        return status;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite( boolean favorite ) {
        this.isFavorite = favorite;
    }

    @Override
    public String toString() {
        return
                "FixturesItem{" +
                        "date = '" + date + '\'' +
                        ",result = '" + result + '\'' +
                        ",_links = '" + links + '\'' +
                        ",matchday = '" + matchday + '\'' +
                        ",odds = '" + odds + '\'' +
                        ",awayTeamName = '" + awayTeamName + '\'' +
                        ",homeTeamName = '" + homeTeamName + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }

}