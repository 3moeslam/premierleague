package main.sparrow.com.premierleague.presentation.splash;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Process;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.jetbrains.annotations.TestOnly;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import main.sparrow.com.premierleague.R;
import main.sparrow.com.premierleague.di.BaseFragment;
import main.sparrow.com.premierleague.presentation.common.DataViewModel;
import main.sparrow.com.premierleague.presentation.home.HomeFragment;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Splash Screen
 * It only transition screen until data loaded from server.
 * After data loading finished it will open {@link HomeFragment}
 */
public class SplashFragment extends BaseFragment {

    @BindView( R.id.logo ) ImageView logoImage;
    @BindView( R.id.progressBar ) ProgressBar progressBar;
    @BindView( R.id.loadingText ) TextView loadingText;
    AlertDialog dialog;

    //ButterKnife Unbinder to avoid memory leaks
    private Unbinder unbinder;
    //Navigation controller <a href="https://developer.android.com/topic/libraries/architecture/navigation/">See More</a>
    private NavController navController;
    private DataViewModel viewModel;
    private Scheduler scheduler = AndroidSchedulers.mainThread ();


    private CompositeDisposable disposables = new CompositeDisposable ();


    /**
     * Used as animation call back to Open {@link HomeFragment}
     */
    private Animation.AnimationListener exitAnimationListener = new Animation.AnimationListener () {
        @Override public void onAnimationStart( Animation animation ) {
        }

        @Override public void onAnimationEnd( Animation animation ) {
            navController.navigate ( R.id.openHomeFragment );
        }

        @Override public void onAnimationRepeat( Animation animation ) {
        }
    };


    public SplashFragment() {
        // Required empty public constructor
    }

    /**
     * Inject View Model and NavController in costructor in testing only to avoid additional complexity in creating koin modules
     *
     * @param viewModel     Mock of view model
     * @param navController Mock of NavController
     */
    @SuppressLint( "ValidFragment" ) @TestOnly SplashFragment( DataViewModel viewModel , NavController navController ) {
        this.navController = navController;
        this.viewModel = viewModel;
    }

    @Override public View onCreateView( @NonNull LayoutInflater inflater , ViewGroup container ,
                                        Bundle savedInstanceState ) {
        //inflate view
        View view = inflater.inflate ( R.layout.fragment_splash , container , false );
        //Inject views
        unbinder = ButterKnife.bind ( this , view );

        return view;
    }

    @Override public void onViewCreated( @NonNull View view , @Nullable Bundle savedInstanceState ) {
        super.onViewCreated ( view , savedInstanceState );

        //Initiate nav controller if not in test mode
        if ( navController == null )
            navController = Navigation.findNavController ( getView () );

        //Initiate viewmodel if not in test mode
        if ( viewModel == null )
            viewModel = getSharedViewModel ( DataViewModel.class );

        startLogoAnimation ();

        viewModel.loadingIndecator
                .observe ( this , this::onLoadingFinished );

        Disposable disposable = viewModel.networkErrorSubject
                .observeOn ( scheduler )
                .subscribe ( this::onError );
        disposables.add ( disposable );
    }

    /**
     * Called when error happen while data loaded from server
     * It will display Alert dialog to tell user there are an error, with two options
     * reload data or terminate app
     * @param message message to display
     */
    private void onError( String message ) {
        dialog = new AlertDialog.Builder ( getContext () )
                .setTitle ( "Sorry for that" )
                .setMessage ( message )
                .setPositiveButton ( "Try again" , ( dialog , which ) -> viewModel.reloadData () )
                .setNegativeButton ( "End" , ( dialog , which ) -> Process.killProcess ( Process.myPid () ) )
                .setCancelable ( false )
                .create ();
        dialog.show ();
    }


    /**
     * Called When data loading finished
     * It will animate logo out By using scale_down anim
     *
     * @param ignorableValue Boolean value emmited from {@link DataViewModel.loadingIndecator}
     */
    private void onLoadingFinished( @SuppressWarnings( "unused" ) Boolean ignorableValue ) {
        //inflate animation
        Animation animation = AnimationUtils.loadAnimation ( getContext () , R.anim.scale_down );

        //set animation listener to navigate to home after finish
        animation.setAnimationListener ( exitAnimationListener );

        //start animation
        logoImage.startAnimation ( animation );
        loadingText.startAnimation ( animation );
        progressBar.startAnimation ( animation );

        //set views visibility to gone because it animate out
        logoImage.setVisibility ( View.GONE );
        loadingText.setVisibility ( View.GONE );
        progressBar.setVisibility ( View.GONE );

    }

    /**
     * Animate Fragment view in with scale_up.xml located in anim res folder
     */
    private void startLogoAnimation() {
        //inflate animation
        Animation animation = AnimationUtils.loadAnimation ( getContext () , R.anim.scale_up );

        //start animation
        logoImage.startAnimation ( animation );
        loadingText.startAnimation ( animation );
        progressBar.startAnimation ( animation );

        //set views visibility to visible because it animate in
        logoImage.setVisibility ( View.VISIBLE );
        loadingText.setVisibility ( View.VISIBLE );
        progressBar.setVisibility ( View.VISIBLE );
    }

    @Override public void onDestroy() {
        //Unbind butterknife to avoid memory leaks
        unbinder.unbind ();
        disposables.clear ();
        super.onDestroy ();
    }
}
