package main.sparrow.com.premierleague.presentation;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.jetbrains.annotations.TestOnly;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import main.sparrow.com.premierleague.R;

/**
 * Application Based on Single activity architecture <a href="https://www.youtube.com/watch?v=2k8x8V77CrU">Single activity architecture</a>
 * So This is the only Activity in Application
 * Its GUI Conatin only two items
 * 1- Fragment Host
 * 2- BottomNavigationView To navigate through screens
 *
 * Activity responsible for navigation between fragments only, also it contain logic to display or hide BottomNavigationView but it controlled only from fragments
 */
public class MainActivity extends AppCompatActivity {

    @BindView( R.id.bottomNavigationView ) BottomNavigationView bottomNavigationView;

    //ButterKnife Unbinder to avoid memory leaks
    private Unbinder unbinder;

    //Navigation controller <a href="https://developer.android.com/topic/libraries/architecture/navigation/">See More</a>
    private NavController navController;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_home );

        //Inject Activity Views
        unbinder = ButterKnife.bind ( this );

        //Set Navigation item selection listener
        bottomNavigationView.setOnNavigationItemSelectedListener ( this::onBottomNavigationSelected );

        navController = Navigation.findNavController ( this, R.id.fragment );
    }

    /**
     * inject navigation controller in activity
     * used only for testing
     * @param navController Mock for {@link NavController}
     */
    @TestOnly void setNavController(NavController navController){
        this.navController = navController;
    }

    /**
     * Call back for BottomNavigationView item selection
     * @param menuItem selected item
     * @return is handled or not (if false item will not apply selection theme)
     */
    boolean onBottomNavigationSelected( MenuItem menuItem ) {
        switch ( menuItem.getItemId () ) {
            //When User click on Home Item
            case R.id.action_home:
                //Navigate to Home only if home item not selected
                if ( !menuItem.isChecked () )
                    navigateTo ( R.id.openHomeFragment );
                break;
            //When user click on favourite item
            case R.id.action_favourite:
                //Navigate to Home only if favourite item not selected
                if ( !menuItem.isChecked () )
                    navigateTo ( R.id.openFavorite );
                break;
        }
        return true;
    }

    /**
     * Navigate to Distination Fragment which located in main_nav.xml in navigation res folder
     * @param distinationID transition id
     */
    private void navigateTo( @IdRes int distinationID ) {
        navController.navigate ( distinationID );
    }

    /**
     * Logic to show bottomNavigationView
     * mainly used from Home Fragment
     */
    public void showBottomView() {
        bottomNavigationView.setVisibility ( View.VISIBLE );
    }


    @Override protected void onDestroy() {
        //Unbind butterknife to avoid memory leaks
        unbinder.unbind ();
        super.onDestroy ();
    }
}
