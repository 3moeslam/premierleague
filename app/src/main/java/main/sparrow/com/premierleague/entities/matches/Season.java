package main.sparrow.com.premierleague.entities.matches;


import com.google.gson.annotations.SerializedName;


public class Season {

    @SerializedName( "currentMatchday" )
    private int currentMatchday;

    @SerializedName( "endDate" )
    private String endDate;

    @SerializedName( "id" )
    private int id;

    @SerializedName( "startDate" )
    private String startDate;

    public int getCurrentMatchday() {
        return currentMatchday;
    }

    public String getEndDate() {
        return endDate;
    }

    public int getId() {
        return id;
    }

    public String getStartDate() {
        return startDate;
    }

    @Override
    public String toString() {
        return
                "Season{" +
                        "currentMatchday = '" + currentMatchday + '\'' +
                        ",endDate = '" + endDate + '\'' +
                        ",id = '" + id + '\'' +
                        ",startDate = '" + startDate + '\'' +
                        "}";
    }
}