package main.sparrow.com.premierleague.domain.database_converters;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import androidx.room.TypeConverter;
import main.sparrow.com.premierleague.entities.matches.Score;


/**
 * Room Type Converter class To Convert form {@link Score} to {@link String} in Json Format
 * We will use Gson to convert Pojo to be able to store it in table
 * Class methods used by Room
 */
public class ScoreConverter {

    /**
     * Convert {@link Score} to {@link String}
     * @param score pojo to convert
     * @return json representation
     */
    @TypeConverter
    public String fromScoreToString( Score score ) {
        if ( score == null ) {
            return (null);
        }
        Gson gson = new Gson ();
        Type type = new TypeToken <Score> () {
        }.getType ();
        return gson.toJson ( score , type );
    }

    /**
     * Convert {@link String} to {@link Score}
     * @param scoreString json representation
     * @return generated from json
     */
    @TypeConverter
    public Score fromStringToScore( String scoreString ) {
        if ( scoreString == null ) {
            return (null);
        }
        Gson gson = new Gson ();
        Type type = new TypeToken <Score> () {
        }.getType ();
        return gson.fromJson ( scoreString , type );
    }

}
