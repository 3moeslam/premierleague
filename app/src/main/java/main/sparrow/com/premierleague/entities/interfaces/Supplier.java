package main.sparrow.com.premierleague.entities.interfaces;

@FunctionalInterface
public interface Supplier <T> {
    T apply();
}
