package main.sparrow.com.premierleague.presentation;

import main.sparrow.com.premierleague.BuildConfig;
import main.sparrow.com.premierleague.di.KApp;
import timber.log.Timber;

/**
 * Application Class.
 * Extends Kotlin Application base class {@link KApp} to be able to integrate with koin DI SDK
 * It must start Koin DI and Timber Logging SDK
 */
public class App extends KApp {

    @Override public void onCreate() {
        super.onCreate ();

        buildTimberTree ();

        startKoin ();
    }


    /**
     * Initialize Timber Logging SDK In debug mode, we can also create Release Tree
     * @see <a href="https://github.com/JakeWharton/timber">Timber</a>
     */
    private void buildTimberTree() {
        if ( BuildConfig.DEBUG ) {
            Timber.plant ( new Timber.DebugTree () );
        }
    }
}
