package main.sparrow.com.premierleague.entities.matches;


import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.TestOnly;


public class Score {

    @SerializedName( "duration" )
    private String duration;

    @SerializedName( "winner" )
    private String winner;

    @SerializedName( "penalties" )
    private Penalties penalties;

    @SerializedName( "halfTime" )
    private HalfTime halfTime;

    @SerializedName( "fullTime" )
    private FullTime fullTime;

    @SerializedName( "extraTime" )
    private ExtraTime extraTime;

    @TestOnly
    public Score( String duration , String winner , Penalties penalties , HalfTime halfTime , FullTime fullTime , ExtraTime extraTime ) {
        this.duration = duration;
        this.winner = winner;
        this.penalties = penalties;
        this.halfTime = halfTime;
        this.fullTime = fullTime;
        this.extraTime = extraTime;
    }

    public String getDuration() {
        return duration;
    }

    public String getWinner() {
        return winner;
    }

    public Penalties getPenalties() {
        return penalties;
    }

    public HalfTime getHalfTime() {
        return halfTime;
    }

    public FullTime getFullTime() {
        return fullTime;
    }

    public ExtraTime getExtraTime() {
        return extraTime;
    }

    @Override
    public String toString() {
        return
                "Score{" +
                        "duration = '" + duration + '\'' +
                        ",winner = '" + winner + '\'' +
                        ",penalties = '" + penalties + '\'' +
                        ",halfTime = '" + halfTime + '\'' +
                        ",fullTime = '" + fullTime + '\'' +
                        ",extraTime = '" + extraTime + '\'' +
                        "}";
    }
}