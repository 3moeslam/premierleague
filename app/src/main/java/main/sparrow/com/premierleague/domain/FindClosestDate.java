package main.sparrow.com.premierleague.domain;


import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import main.sparrow.com.premierleague.entities.matches.MatchesItem;
import kotlin.Pair;
import kotlin.jvm.functions.Function2;


/**
 * Find Index of given day in <code>List &lt; Pair&lt;String, List &lt;MatchesItem>>></code>
 * If given day not exist it will return index of next day
 *
 * @param pairs  List contain item needed
 * @param target target date to search for
 * return index of nearest item
 */
public class FindClosestDate implements Function2 <List <Pair <String, List <MatchesItem>>>, Date, Integer> {

    @Override public Integer invoke( List <Pair <String, List <MatchesItem>>> pairs , Date target ) {
        //initial distance 1 Means day is in past
        int distance = 1;
        Pair <String, List <MatchesItem>> closest = null;
        @SuppressLint( "SimpleDateFormat" ) SimpleDateFormat format = new SimpleDateFormat ( "yyyy-MM-dd" );
        try {
            //format target date to be in same format of server
            String targetDateString = format.format ( target );
            //return target date back to date
            Date targetDate = format.parse ( targetDateString );

            for ( Pair <String, List <MatchesItem>> pair : pairs ) {
                //convert date (in pair) to Date
                Date pairDate = format.parse ( pair.getFirst () );
                //Compare both
                int currentDistance = targetDate.compareTo ( pairDate );
                //check if calculated distance less than stored
                if ( currentDistance < distance ) {
                    //if yes latch it
                    closest = pair;
                    break;
                }
            }
            //after finish get index of closest
            int resultIndex =  pairs.indexOf ( closest );

            //check if closest is exist (Date in not out of list)
            if(resultIndex == -1)
                //if out of list return max index
                return pairs.size () -1;
            else
                return resultIndex;
        } catch ( Throwable throwable ) {
            //In case of parsing fail
            throwable.printStackTrace ();
            return 0;
        }
    }
}
