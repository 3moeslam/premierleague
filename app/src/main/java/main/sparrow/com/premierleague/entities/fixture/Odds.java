package main.sparrow.com.premierleague.entities.fixture;


import com.google.gson.annotations.SerializedName;


public class Odds {

    @SerializedName( "awayWin" )
    private double awayWin;

    @SerializedName( "draw" )
    private double draw;

    @SerializedName( "homeWin" )
    private double homeWin;

    public double getAwayWin() {
        return awayWin;
    }

    public double getDraw() {
        return draw;
    }

    public double getHomeWin() {
        return homeWin;
    }

    @Override
    public String toString() {
        return
                "Odds{" +
                        "awayWin = '" + awayWin + '\'' +
                        ",draw = '" + draw + '\'' +
                        ",homeWin = '" + homeWin + '\'' +
                        "}";
    }
}