package main.sparrow.com.premierleague.entities.matches;


import com.google.gson.annotations.SerializedName;


public class RefereesItem {

    @SerializedName( "nationality" )
    private Object nationality;

    @SerializedName( "name" )
    private String name;

    @SerializedName( "id" )
    private int id;

    public Object getNationality() {
        return nationality;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return
                "RefereesItem{" +
                        "nationality = '" + nationality + '\'' +
                        ",name = '" + name + '\'' +
                        ",id = '" + id + '\'' +
                        "}";
    }
}