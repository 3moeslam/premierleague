package main.sparrow.com.premierleague.entities.fixture;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Response {

    @SerializedName( "_links" )
    private Links links;

    @SerializedName( "count" )
    private int count;

    @SerializedName( "fixtures" )
    private List <FixturesItem> fixtures;

    public Links getLinks() {
        return links;
    }

    public int getCount() {
        return count;
    }

    public List <FixturesItem> getFixtures() {
        return fixtures;
    }

    @Override
    public String toString() {
        return
                "TeamsResponse{" +
                        "_links = '" + links + '\'' +
                        ",count = '" + count + '\'' +
                        ",fixtures = '" + fixtures + '\'' +
                        "}";
    }
}