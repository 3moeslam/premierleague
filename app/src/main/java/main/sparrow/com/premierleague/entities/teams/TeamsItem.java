package main.sparrow.com.premierleague.entities.teams;


import com.google.gson.annotations.SerializedName;


public class TeamsItem {

    @SerializedName( "area" )
    private Area area;

    @SerializedName( "venue" )
    private String venue;

    @SerializedName( "website" )
    private String website;

    @SerializedName( "address" )
    private String address;

    @SerializedName( "crestUrl" )
    private String crestUrl;

    @SerializedName( "tla" )
    private String tla;

    @SerializedName( "founded" )
    private int founded;

    @SerializedName( "lastUpdated" )
    private String lastUpdated;

    @SerializedName( "clubColors" )
    private String clubColors;

    @SerializedName( "phone" )
    private String phone;

    @SerializedName( "name" )
    private String name;

    @SerializedName( "id" )
    private int id;

    @SerializedName( "shortName" )
    private String shortName;

    @SerializedName( "email" )
    private String email;

    public TeamsItem( int id , String name , String crestUrl ) {
        this.crestUrl = crestUrl;
        this.name = name;
        this.id = id;
    }

    public Area getArea() {
        return area;
    }

    public String getVenue() {
        return venue;
    }

    public String getWebsite() {
        return website;
    }

    public String getAddress() {
        return address;
    }

    public String getCrestUrl() {
        return crestUrl;
    }

    public String getTla() {
        return tla;
    }

    public int getFounded() {
        return founded;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public String getClubColors() {
        return clubColors;
    }

    public String getPhone() {
        return phone;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getShortName() {
        return shortName;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return
                "TeamsItem{" +
                        "area = '" + area + '\'' +
                        ",venue = '" + venue + '\'' +
                        ",website = '" + website + '\'' +
                        ",address = '" + address + '\'' +
                        ",crestUrl = '" + crestUrl + '\'' +
                        ",tla = '" + tla + '\'' +
                        ",founded = '" + founded + '\'' +
                        ",lastUpdated = '" + lastUpdated + '\'' +
                        ",clubColors = '" + clubColors + '\'' +
                        ",phone = '" + phone + '\'' +
                        ",name = '" + name + '\'' +
                        ",id = '" + id + '\'' +
                        ",shortName = '" + shortName + '\'' +
                        ",email = '" + email + '\'' +
                        "}";
    }
}