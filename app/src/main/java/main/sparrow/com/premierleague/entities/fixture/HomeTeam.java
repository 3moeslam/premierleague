package main.sparrow.com.premierleague.entities.fixture;


import com.google.gson.annotations.SerializedName;


public class HomeTeam {

    @SerializedName( "href" )
    private String href;

    public String getHref() {
        return href;
    }

    @Override
    public String toString() {
        return
                "HomeTeam{" +
                        "href = '" + href + '\'' +
                        "}";
    }
}