package main.sparrow.com.premierleague.domain.network_gateway;

import main.sparrow.com.premierleague.entities.matches.Response;
import main.sparrow.com.premierleague.entities.teams.TeamsResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;

public interface PremierLeagueApi {

    @GET( "competitions/PL/matches" )
    Observable <Response> getFixture();

    @GET( "competitions/PL/teams" )
    Observable <TeamsResponse> allTeamsInCompetition();
}