@file:JvmName("KoinModule")

package main.sparrow.com.premierleague.di

import android.app.Application
import android.content.Context
import kotlin.Pair
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import main.sparrow.com.premierleague.domain.FindClosestDate
import main.sparrow.com.premierleague.domain.FixtureLoader
import main.sparrow.com.premierleague.domain.TeamsLoader
import main.sparrow.com.premierleague.domain.network_gateway.PremierLeagueApi
import main.sparrow.com.premierleague.entities.matches.MatchesItem
import main.sparrow.com.premierleague.entities.teams.TeamsItem
import main.sparrow.com.premierleague.domain.database_gateway.Dao
import main.sparrow.com.premierleague.frameworks.database.DatabaseConstructor
import main.sparrow.com.premierleague.frameworks.network.ApiConstructor
import main.sparrow.com.premierleague.presentation.common.DataViewModel
import io.reactivex.Observable
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.get
import org.koin.android.ext.android.startKoin
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.ext.android.getViewModelByClass
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import org.koin.standalone.StandAloneContext
import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 * Koin Module That will provide all instances needed
 */
@JvmField
val appModule = module {
	single<PremierLeagueApi> { ApiConstructor().apply() }
	single<Dao> { DatabaseConstructor().apply(androidContext()) }

	factory<Function<PremierLeagueApi, Observable<Pair<String, Iterable<MatchesItem>>>>>("FixtureLoader") { FixtureLoader() }
	factory<Function<PremierLeagueApi, Observable<TeamsItem>>>("TeamLoader") { TeamsLoader() }

	factory<Any>("findClosestDate") { FindClosestDate() }

	factory { Schedulers.io() }
	factory<Executor> { Executors.newSingleThreadExecutor() }

	factory { androidContext().getSharedPreferences("MainPrefrences", Context.MODE_PRIVATE) }

	viewModel { DataViewModel(get(), get(), get("FixtureLoader"), get("TeamLoader"), get(), get(), get()) }
}


/**
 * Base Fragment written in kotlin as Hack to be able to get injected view model from koin di
 * Should not contain any other logic
 */
open class BaseFragment : Fragment() {

	/**
	 * get injected object by name
	 * <p><font color="blue">
	 * It's important to use <code>factory<Any></code> when you need to get injection only by name
 	 * </p>
	 * @param name object name
	 * @return object instance in type of <code> Object </code>, do not forget casting :)
	 */
	fun injectValByName(name :String) :Any{
		return get(name)
	}
	/**
	 * Get injected view model from koin
	 * It Linked with fragment so it is not shareable
	 * @param klass ViewModel class
	 */
	fun <T : ViewModel> getInjectedViewModel(klass: Class<T>): T {
		val kClass = klass.kotlin
		return getViewModelByClass(kClass)
	}

	/**
	 * Get injected view model from koin
	 * It Linked with activity not fragment so it is shareable
	 * @param klass ViewModel class
	 */
	fun <T : ViewModel> getSharedViewModel(klass: Class<T>): T {
		val kClass = klass.kotlin
		val from = { activity ?: error("Parent activity should not be null") }
		return getViewModelByClass(kClass, from = from)
	}
}

open class KApp : Application() {

	fun startKoin() {
		startKoin(this, listOf(appModule))
	}

	fun stopKoin(){
		StandAloneContext.stopKoin()
	}
}