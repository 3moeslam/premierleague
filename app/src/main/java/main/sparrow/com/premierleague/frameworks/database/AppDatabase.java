package main.sparrow.com.premierleague.frameworks.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import main.sparrow.com.premierleague.domain.database_gateway.Dao;
import main.sparrow.com.premierleague.entities.matches.MatchesItem;

@Database( entities = {MatchesItem.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract Dao getDao();
}
