package main.sparrow.com.premierleague.entities.matches;


import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.TestOnly;


public class AwayTeam {

    @TestOnly
    public AwayTeam( String name , int id ) {
        this.name = name;
        this.id = id;
    }

    @SerializedName( "name" )
    private String name;

    @SerializedName( "id" )
    private int id;

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return
                "AwayTeam{" +
                        "name = '" + name + '\'' +
                        ",id = '" + id + '\'' +
                        "}";
    }
}