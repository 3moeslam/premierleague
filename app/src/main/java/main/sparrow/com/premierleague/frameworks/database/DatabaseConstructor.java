package main.sparrow.com.premierleague.frameworks.database;

import android.content.Context;

import androidx.room.Room;
import main.sparrow.com.premierleague.domain.database_gateway.Dao;
import io.reactivex.functions.Function;

/**
 * Database constructor
 * Used to build Dao (Database gateway in Domain)
 * Simply it build Room Database.
 * Input: Android Context
 * Output: Dao Implementation
 */
public class DatabaseConstructor implements Function <Context, Dao> {

    @Override public Dao apply( Context context ) {
        return Room.databaseBuilder ( context , AppDatabase.class , "database" )
                .fallbackToDestructiveMigration ()
                .build ()
                .getDao ();
    }
}
