package main.sparrow.com.premierleague.entities.teams;


import com.google.gson.annotations.SerializedName;


public class Competition{

	@SerializedName("area")
	private Area area;

	@SerializedName("lastUpdated")
	private String lastUpdated;

	@SerializedName("code")
	private String code;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("plan")
	private String plan;

	public Area getArea(){
		return area;
	}

	public String getLastUpdated(){
		return lastUpdated;
	}

	public String getCode(){
		return code;
	}

	public String getName(){
		return name;
	}

	public int getId(){
		return id;
	}

	public String getPlan(){
		return plan;
	}

	@Override
 	public String toString(){
		return 
			"Competition{" + 
			"area = '" + area + '\'' + 
			",lastUpdated = '" + lastUpdated + '\'' + 
			",code = '" + code + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",plan = '" + plan + '\'' + 
			"}";
		}
}