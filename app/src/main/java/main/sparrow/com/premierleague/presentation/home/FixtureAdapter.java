package main.sparrow.com.premierleague.presentation.home;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import main.sparrow.com.premierleague.R;
import main.sparrow.com.premierleague.entities.matches.MatchesItem;
import main.sparrow.com.premierleague.presentation.common.MatchesAdapter;
import io.reactivex.subjects.PublishSubject;
import kotlin.Pair;

/**
 * FixtureAdapter
 * Recycler view adapter used to dispaly List of days came
 * each item in this adapter include recycler view to display matches of this day
 * This adapter used in home screen
 */
public class FixtureAdapter extends RecyclerView.Adapter <FixtureAdapter.DayViewHolder> {

    private List <Pair <String, List <MatchesItem>>> pairs;
    private PublishSubject<MatchesItem> favoriteClickChannel;

    FixtureAdapter( List <Pair <String, List <MatchesItem>>> pairs , PublishSubject<MatchesItem> favoriteClickChannel ) {
        this.pairs = pairs;
        this.favoriteClickChannel = favoriteClickChannel;
    }

    @NonNull @Override public DayViewHolder onCreateViewHolder( @NonNull ViewGroup parent , int viewType ) {
        // inflate fixture_day.xml layout file
        View view = LayoutInflater.from ( parent.getContext () )
                .inflate ( R.layout.fixture_day , parent , false );

        //create and return DayViewHolder
        return new DayViewHolder ( view );
    }

    @Override public void onBindViewHolder( @NonNull DayViewHolder holder , int position ) {
        //get day from pair list
        Pair <String, List <MatchesItem>> day = pairs.get ( position );
        //add spaces to date and display it
        holder.dayOfWeek.setText ( day.getFirst ().replace ( "-"," - " ) );

        // Create MatchesAdapter for current date to display
        MatchesAdapter matchesAdapter = new MatchesAdapter ( day.getSecond () , null , favoriteClickChannel );
        holder.dayRecycler.setAdapter ( matchesAdapter );
        holder.dayRecycler.setLayoutManager ( new LinearLayoutManager ( holder.dayRecycler.getContext () ) );
    }

    @Override public int getItemCount() {
        return pairs.size ();
    }

    /**
     * Recycler view holder contain views from fixture_day.xml layout file
     */
    static class DayViewHolder extends RecyclerView.ViewHolder {

        @BindView( R.id.dayDate ) TextView dayOfWeek;
        @BindView( R.id.fixtureDay ) RecyclerView dayRecycler;

        DayViewHolder( @NonNull View itemView ) {
            super ( itemView );
            ButterKnife.bind ( this , itemView );
        }
    }
}
