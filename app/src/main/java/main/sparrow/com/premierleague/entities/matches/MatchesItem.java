package main.sparrow.com.premierleague.entities.matches;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.TestOnly;

import java.util.List;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
import main.sparrow.com.premierleague.domain.database_converters.AwayTeamConverter;
import main.sparrow.com.premierleague.domain.database_converters.HomeTeamConverter;
import main.sparrow.com.premierleague.domain.database_converters.ScoreConverter;


@Entity( tableName = "FavouriteMatches" )
public class MatchesItem {

    @SerializedName( "lastUpdated" )
    private String lastUpdated;

    @SerializedName( "score" )
    @TypeConverters(ScoreConverter.class)
    private Score score;

    @SerializedName( "stage" )
    private String stage;

    @SerializedName( "matchday" )
    private int matchday;

    @SerializedName( "awayTeam" )
    @TypeConverters(AwayTeamConverter.class)
    private AwayTeam awayTeam;

    @SerializedName( "season" )
    @Ignore
    private Season season;

    @SerializedName( "homeTeam" )
    @TypeConverters(HomeTeamConverter.class)
    private HomeTeam homeTeam;

    @SerializedName( "id" )
    @PrimaryKey
    private int id;

    @SerializedName( "utcDate" )
    private String utcDate;

    @SerializedName( "referees" )
    @Ignore
    private List <RefereesItem> referees;

    @SerializedName( "status" )
    private String status;

    @SerializedName( "group" )
    private String group;

    private boolean isFavorite;

    public MatchesItem() {
    }

    @TestOnly public MatchesItem( String lastUpdated , Score score , String stage , int matchday , AwayTeam awayTeam , Season season , HomeTeam homeTeam , int id , String utcDate , List <RefereesItem> referees , String status , String group , boolean isFavorite ) {
        this.lastUpdated = lastUpdated;
        this.score = score;
        this.stage = stage;
        this.matchday = matchday;
        this.awayTeam = awayTeam;
        this.season = season;
        this.homeTeam = homeTeam;
        this.id = id;
        this.utcDate = utcDate;
        this.referees = referees;
        this.status = status;
        this.group = group;
        this.isFavorite = isFavorite;
    }


    public String getLastUpdated() {
        return lastUpdated;
    }

    public Score getScore() {
        return score;
    }

    public String getStage() {
        return stage;
    }

    public int getMatchday() {
        return matchday;
    }

    public AwayTeam getAwayTeam() {
        return awayTeam;
    }

    public Season getSeason() {
        return season;
    }

    public HomeTeam getHomeTeam() {
        return homeTeam;
    }

    public int getId() {
        return id;
    }

    public String getUtcDate() {
        return utcDate;
    }

    public List <RefereesItem> getReferees() {
        return referees;
    }

    public String getStatus() {
        return status;
    }

    public String getGroup() {
        return group;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite( boolean favorite ) {
        isFavorite = favorite;
    }


    public void setLastUpdated( String lastUpdated ) {
        this.lastUpdated = lastUpdated;
    }

    public void setScore( Score score ) {
        this.score = score;
    }

    public void setStage( String stage ) {
        this.stage = stage;
    }

    public void setMatchday( int matchday ) {
        this.matchday = matchday;
    }

    public void setAwayTeam( AwayTeam awayTeam ) {
        this.awayTeam = awayTeam;
    }

    public void setSeason( Season season ) {
        this.season = season;
    }

    public void setHomeTeam( HomeTeam homeTeam ) {
        this.homeTeam = homeTeam;
    }

    public void setId( int id ) {
        this.id = id;
    }

    public void setUtcDate( String utcDate ) {
        this.utcDate = utcDate;
    }

    public void setReferees( List <RefereesItem> referees ) {
        this.referees = referees;
    }

    public void setStatus( String status ) {
        this.status = status;
    }

    public void setGroup( String group ) {
        this.group = group;
    }

    @Override
    public String toString() {
        return
                "MatchesItem{" +
                        "lastUpdated = '" + lastUpdated + '\'' +
                        ",score = '" + score + '\'' +
                        ",stage = '" + stage + '\'' +
                        ",matchday = '" + matchday + '\'' +
                        ",awayTeam = '" + awayTeam + '\'' +
                        ",season = '" + season + '\'' +
                        ",homeTeam = '" + homeTeam + '\'' +
                        ",id = '" + id + '\'' +
                        ",utcDate = '" + utcDate + '\'' +
                        ",referees = '" + referees + '\'' +
                        ",status = '" + status + '\'' +
                        ",group = '" + group + '\'' +
                        "}";
    }
}