package main.sparrow.com.premierleague.presentation.favorite;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.jetbrains.annotations.TestOnly;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import main.sparrow.com.premierleague.R;
import main.sparrow.com.premierleague.di.BaseFragment;
import main.sparrow.com.premierleague.entities.matches.MatchesItem;
import main.sparrow.com.premierleague.presentation.common.DataViewModel;
import main.sparrow.com.premierleague.presentation.common.MatchesAdapter;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;

/**
 * Favorite Fragment
 * Display Favorite list from database
 * it uses shared view model (Linked to activity)
 */
public class FavoriteFragment extends BaseFragment {

    //Main Recycler view
    @BindView( R.id.favoriteRecycler ) RecyclerView favoriteRecycler;
    // Error text only shown if favorite list is empty
    @BindView( R.id.errorText ) TextView errorText;

    //favorite Click Channel to handle favorite button click
    private PublishSubject <MatchesItem> favoriteClickChannel = PublishSubject.create ();

    //Butterknife unbinder to avoid memory leaks
    private Unbinder unbinder;
    //Instance of view model
    private DataViewModel viewModel;

    private CompositeDisposable disposables = new CompositeDisposable ();

    public FavoriteFragment() {
        // Required empty public constructor
    }

    @SuppressLint( "ValidFragment" ) @TestOnly FavoriteFragment( DataViewModel viewModel ,PublishSubject <MatchesItem> favoriteClickChannel ) {
        this.viewModel = viewModel;
        this.favoriteClickChannel = favoriteClickChannel;
    }

    @Override
    public View onCreateView( @NonNull LayoutInflater inflater , ViewGroup container ,
                              Bundle savedInstanceState ) {
        View view = inflater.inflate ( R.layout.fragment_favorite , container , false );
        unbinder = ButterKnife.bind ( this , view );
        return view;
    }

    @Override public void onViewCreated( @NonNull View view , @Nullable Bundle savedInstanceState ) {
        super.onViewCreated ( view , savedInstanceState );

        //do not initialize view model only in test mode
        if ( viewModel == null )
            viewModel = getSharedViewModel ( DataViewModel.class );

        // load favorite from sqlite
        viewModel.getFavorite ().observe ( this , this::onFavoriteChanges );
        // listen to favorite channel
        Disposable disposable = favoriteClickChannel.subscribe ( this::onFavoriteItemClicked );
        disposables.add ( disposable );
    }

    /**
     * Called When item need to remove from favorite
     *
     * @param matchesItem item to remove
     */
    private void onFavoriteItemClicked( MatchesItem matchesItem ) {
        viewModel.handleFavoriteClick ( matchesItem );
    }

    /**
     * Called when match items is ready
     *
     * @param matchesItems match items list
     */
    private void onFavoriteChanges( List <MatchesItem> matchesItems ) {
        //check if list not empty
        if ( matchesItems.size () > 0 ) {
            // list not empty, Initialize Adapter
            MatchesAdapter matchesAdapter = new MatchesAdapter ( matchesItems , null , favoriteClickChannel );
            favoriteRecycler.setAdapter ( matchesAdapter );
            favoriteRecycler.setLayoutManager ( new LinearLayoutManager ( getContext () ) );
        } else {
            // Empty list show error text
            favoriteRecycler.setVisibility ( View.GONE );
            errorText.setVisibility ( View.VISIBLE );

        }
    }


    @Override public void onDestroy() {
        //Unbind butter knife
        unbinder.unbind ();
        //Clear disposables
        disposables.clear ();
        //complete favorite channel
        favoriteClickChannel.onComplete ();
        super.onDestroy ();
    }
}
