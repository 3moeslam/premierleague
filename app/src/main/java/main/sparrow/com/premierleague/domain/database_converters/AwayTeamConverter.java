package main.sparrow.com.premierleague.domain.database_converters;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import androidx.room.TypeConverter;
import main.sparrow.com.premierleague.entities.matches.AwayTeam;

/**
 * Room Type Converter class To Convert form {@link AwayTeam} to {@link String} in Json Format
 * We will use Gson to convert Pojo to be able to store it in table
 * Class methods used by Room
 */
public class AwayTeamConverter {

    /**
     * Convert {@link AwayTeam} to {@link String}
     *
     * @param awayTeam pojo to convert
     * @return json representation
     */
    @TypeConverter
    public String fromAwayTeamToString( AwayTeam awayTeam ) {
        if ( awayTeam == null ) {
            return (null);
        }
        Gson gson = new Gson ();
        Type type = new TypeToken <AwayTeam> () {
        }.getType ();
        return gson.toJson ( awayTeam , type );
    }

    /**
     * Convert {@link String} to {@link AwayTeam}
     *
     * @param awayTeamString json representation
     * @return {@link AwayTeam} generated from json
     */
    @TypeConverter
    public AwayTeam fromStringToHomeTeam( String awayTeamString ) {
        if ( awayTeamString == null ) {
            return (null);
        }
        Gson gson = new Gson ();
        Type type = new TypeToken <AwayTeam> () {
        }.getType ();
        return gson.fromJson ( awayTeamString , type );
    }
}
