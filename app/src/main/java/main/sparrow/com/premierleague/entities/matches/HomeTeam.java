package main.sparrow.com.premierleague.entities.matches;


import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.TestOnly;


public class HomeTeam {

    @SerializedName( "name" )
    private String name;

    @SerializedName( "id" )
    private int id;

    @TestOnly
    public HomeTeam( String name , int id ) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return
                "HomeTeam{" +
                        "name = '" + name + '\'' +
                        ",id = '" + id + '\'' +
                        "}";
    }
}