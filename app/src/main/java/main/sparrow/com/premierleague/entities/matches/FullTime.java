package main.sparrow.com.premierleague.entities.matches;


import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.TestOnly;


public class FullTime {

    @SerializedName( "awayTeam" )
    private int awayTeam;

    @SerializedName( "homeTeam" )
    private int homeTeam;

    @TestOnly
    public FullTime( int awayTeam , int homeTeam ) {
        this.awayTeam = awayTeam;
        this.homeTeam = homeTeam;
    }

    public int getAwayTeam() {
        return awayTeam;
    }

    public int getHomeTeam() {
        return homeTeam;
    }

    @Override
    public String toString() {
        return
                "FullTime{" +
                        "awayTeam = '" + awayTeam + '\'' +
                        ",homeTeam = '" + homeTeam + '\'' +
                        "}";
    }
}