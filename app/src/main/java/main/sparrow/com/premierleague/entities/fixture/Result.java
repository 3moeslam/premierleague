package main.sparrow.com.premierleague.entities.fixture;


import com.google.gson.annotations.SerializedName;


public class Result {

    @SerializedName( "goalsHomeTeam" )
    private int goalsHomeTeam;

    @SerializedName( "goalsAwayTeam" )
    private int goalsAwayTeam;

    @SerializedName( "halfTime" )
    private HalfTime halfTime;

    public int getGoalsHomeTeam() {
        return goalsHomeTeam;
    }

    public int getGoalsAwayTeam() {
        return goalsAwayTeam;
    }

    public HalfTime getHalfTime() {
        return halfTime;
    }

    @Override
    public String toString() {
        return
                "Result{" +
                        "goalsHomeTeam = '" + goalsHomeTeam + '\'' +
                        ",goalsAwayTeam = '" + goalsAwayTeam + '\'' +
                        ",halfTime = '" + halfTime + '\'' +
                        "}";
    }
}