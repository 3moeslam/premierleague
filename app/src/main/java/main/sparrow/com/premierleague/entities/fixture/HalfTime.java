package main.sparrow.com.premierleague.entities.fixture;


import com.google.gson.annotations.SerializedName;


public class HalfTime {

    @SerializedName( "goalsHomeTeam" )
    private int goalsHomeTeam;

    @SerializedName( "goalsAwayTeam" )
    private int goalsAwayTeam;

    public int getGoalsHomeTeam() {
        return goalsHomeTeam;
    }

    public int getGoalsAwayTeam() {
        return goalsAwayTeam;
    }

    @Override
    public String toString() {
        return
                "HalfTime{" +
                        "goalsHomeTeam = '" + goalsHomeTeam + '\'' +
                        ",goalsAwayTeam = '" + goalsAwayTeam + '\'' +
                        "}";
    }
}