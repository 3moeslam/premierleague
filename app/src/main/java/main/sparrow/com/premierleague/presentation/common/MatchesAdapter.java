package main.sparrow.com.premierleague.presentation.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import main.sparrow.com.premierleague.R;
import main.sparrow.com.premierleague.entities.matches.MatchesItem;
import main.sparrow.com.premierleague.presentation.GlideApp;
import main.sparrow.com.premierleague.presentation.GlideOptions;
import io.reactivex.subjects.PublishSubject;

/**
 * MatchesAdapter
 * Used To Display list of matches
 * Mainly it has 2 Uses in project (HomeScreen to Display matches of day, Favorite to display favorite list)
 */
public class MatchesAdapter extends RecyclerView.Adapter <MatchesAdapter.FixtureViewHolder> {

    //List of Match items
    private List <MatchesItem> fixturesItems;
    //Shared pref to get cached team image
    private SharedPreferences sharedPreferences;
    //Favorite channel
    private PublishSubject <MatchesItem> favoriteClickChannel;


    /**
     * MatchesAdapter Used To Display list of matches
     * @param fixturesItems List of Match items
     * @param sharedPreferences Shared pref to get cached team image
     * @param favoriteClickChannel Favorite channel to emmit favorite item
     */
    public MatchesAdapter( List <MatchesItem> fixturesItems , SharedPreferences sharedPreferences , PublishSubject <MatchesItem> favoriteClickChannel ) {
        this.fixturesItems = fixturesItems;
        this.sharedPreferences = sharedPreferences;
        this.favoriteClickChannel = favoriteClickChannel;
    }

    @NonNull @Override public FixtureViewHolder onCreateViewHolder( @NonNull ViewGroup parent , int viewType ) {
        //Initialize shared prefrences if not initialized
        if ( sharedPreferences == null )
            sharedPreferences = parent.getContext ().getSharedPreferences ( "MainPrefrences" , Context.MODE_PRIVATE );

        //Inflate view
        View view = LayoutInflater.from ( parent.getContext () ).inflate ( R.layout.fixture_item , parent , false );
        //Create view holder and return it
        return new FixtureViewHolder ( view );
    }

    @Override public void onBindViewHolder( @NonNull FixtureViewHolder holder , int position ) {
        //Get MatchesItem for current position
        MatchesItem fixture = fixturesItems.get ( position );

        //Set Teams ames
        holder.homeTeamName.setText ( fixture.getHomeTeam ().getName () );
        holder.awayTeamName.setText ( fixture.getAwayTeam ().getName () );

        //if Match played show score else show time
        String status = fixture.getStatus ().equals ( "FINISHED" ) ? getScore ( fixture ) : getTime ( fixture );
        holder.statusText.setText ( status );

        //get teams logo from shared pref
        String homeTeamLogoUrl = sharedPreferences.getString ( fixture.getHomeTeam ().getName () , "" );
        String awayTeamLogoUrl = sharedPreferences.getString ( fixture.getAwayTeam ().getName () , "" );

        //load teams logos
        loadImage ( holder.homeLogo , homeTeamLogoUrl );
        loadImage ( holder.awayLogo , awayTeamLogoUrl );

        //attach listener to favoriteIcon
        holder.favoriteIcon.setOnClickListener ( v -> favoriteClickChannel.onNext ( fixture ) );
    }

    /**
     * return formated time from match item
     * @param fixture fixture item
     * @return formatted time
     */
    private String getTime( MatchesItem fixture ) {
        return fixture.getUtcDate ().split ( "T" )[ 1 ].replace ( ":00Z" , "" );
    }

    /**
     * return formatted final match result
     * @param fixture fixture item
     * @return formatted final result
     */
    private String getScore( MatchesItem fixture ) {
        return fixture.getScore ().getFullTime ().getHomeTeam () + " - " + fixture.getScore ().getFullTime ().getAwayTeam ();
    }


    /**
     * Load Image (PNG or SVG) from url using glide library and
     * SvgGlidePlugins
     * @see https://github.com/kirich1409/SvgGlidePlugins
     * @param img ImageView to display logo on it
     * @param url logo url
     */
    private void loadImage( ImageView img , String url ) {
        GlideApp.with ( img.getContext () )
                .load ( url )
                .apply ( GlideOptions.fitCenterTransform () )
                .into ( img );
    }

    @Override public int getItemCount() {
        return fixturesItems.size ();
    }


    /**
     * Recycler view holder contain views from fixture_item.xml layout file
     */
    static class FixtureViewHolder extends RecyclerView.ViewHolder {

        @BindView( R.id.container ) ConstraintLayout container;
        @BindView( R.id.homeTeamName ) TextView homeTeamName;
        @BindView( R.id.awayTeamName ) TextView awayTeamName;
        @BindView( R.id.statusText ) TextView statusText;
        @BindView( R.id.homeLogo ) ImageView homeLogo;
        @BindView( R.id.awayLogo ) ImageView awayLogo;
        @BindView( R.id.favoriteIcon ) ImageButton favoriteIcon;

        FixtureViewHolder( @NonNull View itemView ) {
            super ( itemView );

            ButterKnife.bind ( this , itemView );
        }
    }
}
