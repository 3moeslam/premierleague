package main.sparrow.com.premierleague.presentation.home;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.jetbrains.annotations.TestOnly;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import main.sparrow.com.premierleague.R;
import main.sparrow.com.premierleague.di.BaseFragment;
import main.sparrow.com.premierleague.entities.matches.MatchesItem;
import main.sparrow.com.premierleague.presentation.MainActivity;
import main.sparrow.com.premierleague.presentation.common.DataViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;
import kotlin.Pair;
import kotlin.jvm.functions.Function2;

/**
 * Home Screen in application
 * It Displays Matches (Fixtures) loaded from server by useing shared view model (Bound with activity) which used in splash to load data
 * - Fixtures arranged by day (Business requiremen) so app use two adapters
 * 1- To display list for day fixture
 * 2- To Display Matches for day
 * - When user Click on Favorite (Which exist in Matches adapter) fragment will recive action using PublishSubject
 * - Show Bottom navigation view when Home Start
 */
public class HomeFragment extends BaseFragment {

    @BindView( R.id.fixtureRecycler ) RecyclerView fixtureRecycler;

    PublishSubject <MatchesItem> favoriteClickChannel = PublishSubject.create ();

    //Inejct FindClosestDate Use case
    private Function2 <List <Pair <String, List <MatchesItem>>>, Date, Integer> findClosestDate;

    //View Model
    private DataViewModel viewModel;
    private List <Pair <String, List <MatchesItem>>> pairs;


    private CompositeDisposable disposables = new CompositeDisposable ();
    private Unbinder unbinder;

    //Show bottom view runnable for test only
    Runnable showBottomViewTest;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Inject View Model and findClosestDate in costructor in testing only to avoid additional complexity in creating koin modules
     *
     * @param viewModel       Mock of view model
     * @param findClosestDate Mock of Function2 <List <Pair <String, List <MatchesItem>>>, Date, Integer>
     */
    @TestOnly HomeFragment( DataViewModel viewModel ,
                            Function2 <List <Pair <String, List <MatchesItem>>>, Date, Integer> findClosestDate ,
                            Runnable showBottomViewTest ) {
        this.viewModel = viewModel;
        this.findClosestDate = findClosestDate;
        this.showBottomViewTest = showBottomViewTest;
    }

    @Override
    public View onCreateView( LayoutInflater inflater , ViewGroup container ,
                              Bundle savedInstanceState ) {
        //Inflate View
        View view = inflater.inflate ( R.layout.fragment_home , container , false );
        //Inject Views
        unbinder = ButterKnife.bind ( this , view );
        return view;
    }

    @Override public void onViewCreated( @NonNull View view , @Nullable Bundle savedInstanceState ) {
        super.onViewCreated ( view , savedInstanceState );

        //Get Shared ViewModel From Koin if not in test mode
        if ( viewModel == null )
            viewModel = getSharedViewModel ( DataViewModel.class );

        //Get findClosestDateFrom Koin if not in test mode
        if ( findClosestDate == null )
            findClosestDate = ( Function2 <List <Pair <String, List <MatchesItem>>>, Date, Integer> ) injectValByName ( "findClosestDate" );

        //Initialize Recycler view
        setupRecycler ( viewModel.list.getValue () );

        showBottomView ();


        viewModel.favoriteMessage
                .observeOn ( AndroidSchedulers.mainThread () )
                .subscribe (  this::onFavoriteMessage );

        //Observe Favorite Button Clicks
        Disposable disposable = favoriteClickChannel.observeOn ( AndroidSchedulers.mainThread () )
                .subscribe ( this::onMatchItemFaviorate );
        disposables.add ( disposable );
    }

    /**
     * Show Bottom View, its orignal state is GONE
     * In testing it will run injected runnable else it will invoke showBottomView from MainActivity
     */
    private void showBottomView() {
        if ( showBottomViewTest == null )
            (( MainActivity ) getActivity ()).showBottomView ();
        else
            showBottomViewTest.run ();
    }

    /**
     * Observer for Favorite message
     *
     * @param message message to display
     */
    private void onFavoriteMessage( String message ) {
        Toast.makeText ( getContext () , message , Toast.LENGTH_LONG ).show ();
    }

    /**
     * Observer for Favorite Button Clicks
     *
     * @param matchesItem clicked item to add or remove from favorite
     */
    private void onMatchItemFaviorate( MatchesItem matchesItem ) {
        viewModel.handleFavoriteClick ( matchesItem );
    }

    /**
     * Initializr Recycler view from pairs loaded from server
     *
     * @param pairs loaded pairs
     */
    private void setupRecycler( List <Pair <String, List <MatchesItem>>> pairs ) {
        //Add Pairs to Class Variable to use them when search to closest date
        this.pairs = pairs;

        //Initialize Recycler
        FixtureAdapter dayAdapter = new FixtureAdapter ( pairs , favoriteClickChannel );
        fixtureRecycler.setAdapter ( dayAdapter );
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager ( getContext () );
        fixtureRecycler.setLayoutManager ( layoutManager );

        //Scroll to taget date, Background running
        scrollToCurrentDate ();
    }

    /**
     * Invoke findClosestDate Usecase, then scroll to needed item
     */
    private void scrollToCurrentDate() {
        Date currentDate = Calendar.getInstance ().getTime ();
        int targetPosition = findClosestDate.invoke ( pairs , currentDate );
        fixtureRecycler.getLayoutManager ().scrollToPosition ( targetPosition );
    }


    @Override public void onDestroy() {
        //Clear Disposables
        disposables.clear ();
        //Close Channel
        favoriteClickChannel.onComplete ();
        //Unbpund to avoid memory leaks
        unbinder.unbind ();

        super.onDestroy ();
    }
}
