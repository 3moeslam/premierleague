package main.sparrow.com.premierleague.entities.fixture;


import com.google.gson.annotations.SerializedName;


public class Links {

    @SerializedName( "awayTeam" )
    private AwayTeam awayTeam;

    @SerializedName( "self" )
    private Self self;

    @SerializedName( "homeTeam" )
    private HomeTeam homeTeam;

    @SerializedName( "competition" )
    private Competition competition;

    public AwayTeam getAwayTeam() {
        return awayTeam;
    }

    public Self getSelf() {
        return self;
    }

    public HomeTeam getHomeTeam() {
        return homeTeam;
    }

    public Competition getCompetition() {
        return competition;
    }

    @Override
    public String toString() {
        return
                "Links{" +
                        "awayTeam = '" + awayTeam + '\'' +
                        ",self = '" + self + '\'' +
                        ",homeTeam = '" + homeTeam + '\'' +
                        ",competition = '" + competition + '\'' +
                        "}";
    }
}