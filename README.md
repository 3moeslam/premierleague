# Premier League App
Android Application displays information about The English Premier League

## Design 
Application Design philosophy taken from ( Clean Architecture By Robert Marten ).
Design contain 5 Layers
1. DI ( Dependency injection ) outer circle
2. Frameworks ( Network , Database ) 
3. Presentation ( Application class, Activities , fragments )
4. Domain ( Gatewayes to Connect with other layers ,  UseCases )
5. Entities ( Only Pojos )


### Techinical Specs for each layer

#### DI ( Dependency injection )
The Most popular DI sdks in Android is Dagger, i think it is not better choices for the following reasons
1. Complex (In implementation)
2. Depend on annotation processor, so it increase compile time.
3. poor error indicator when compile fail.

In other hand we have [Koin](https://insert-koin.io/ "Koin Home") DI sdk  with no reflection, no code generation.
Koin built by kotlin and taget kotlin at first, but they support java too.

In this Layer i used Koin with minimum kotlin code (Only few lines)

To Add Item to be injectable 
1. open  `KoinModule` 
2. Inside `module { ... }` you can add anything you need to inject
    - use `factory { ... }` to inject new instance each time.
    - use `single { ... }` to keep unique instance.
    - use `viewModel { ... }` to create injectable inctance for view  model
3. To Inject Instance (not viewmodel) inside Java Class use KoinJavaComponent.inject method it return lazy object
for ex:
`    Lazy<PremierLeagueApi> api = inject ( PremierLeagueApi.class );
`
4. To Inject ViewModel i created simple Hack by Creating Kotlin Base Class include only one method that's retrive view model.
`
getInjectedViewModel ( SplashViewModel.class );
`
I think by using koin i decrease Complexty of implementation process and compilation time.


#### Frameworks (Network)
##### 1- database
Include all necessary classes to build SQLITE Database using Room library (https://developer.android.com/topic/libraries/architecture/room)
Input : Android Context
Output : Implementation of Domain layer Database Gateway interface
##### 2- network
Include all necessary classes to build Domain Network Gateway Interface, it use Retrofit (https://square.github.io/retrofit/) 
and okhttp3 (http://square.github.io/okhttp/).
Output : Implementation of Domain layer Network Gateway interface.


#### Presentation
Application Presentation layer Application Based on [Single activity architecture](https://www.youtube.com/watch?v=2k8x8V77CrU "Single activity architecture") recommened by google,
So main screen is fragments, architecture of each screen based on MVVM architectural pattern [Guide to app architecture](https://developer.android.com/jetpack/docs/guide "jetpack guide")

All Screens (Activity or fragments) used [ButterKnife](http://jakewharton.github.io/butterknife/) to Inject its views.

##### 1- App class
Extends Kotlin Application base class KApp to be able to integrate with koin DI SDK  It will start Koin DI and Timber Logging SDK
##### 2- MainActivity Class
Activity responsible for navigation between fragments only, 
also it contain logic to display or hide BottomNavigationView but it controlled only from fragments

##### 3- Splash Fragment
Splash Screen
It only transition screen until data loaded from server.
After data loading finished it will open HomeFragment

##### 4- HomeFragment
Home Screen in application
It Displays Matches (Fixtures) loaded from server by useing shared view model (Bound with activity) which used in splash to load data
 - Fixtures arranged by day (Business requiremen) so app use two adapters
   1- To display list for day fixture
   2- To Display Matches for day
 - When user Click on Favorite (Which exist in Matches adapter) fragment will recive action using PublishSubject
 - Show Bottom navigation view when Home Start
##### 5- FixtureAdapter
FixtureAdapter
Recycler view adapter used to dispaly List of days came each item in this adapter include recycler view to display matches of this day,
This adapter used in home screen
##### 6- MatchesAdapter
Used To Display list of matches Mainly it has 2 Uses in project (HomeScreen to Display matches of day, Favorite to display favorite list)
##### 7- FavoriteFragment
Display Favorite list from database it uses shared view model (Linked to activity)
##### 8- DataViewModel
Main View Model used by all fragments because it know how to load data from server and sqlite
Functionalities
 1- Load matches data when start
 2- Load teams data and cache team logo in shared prefrences to load it fast in adapter
 3- Load Favorite item from database
 4- Add Favorite item to database

#### Domain
All usecase It Implements Functional interfaces to reach higher level in abstraction
so it can be replaced with changing class name in di without anything else,
also to achieve SRP (single responsibility principle), 
and it will be helpful in unit testing by mocking it easily.

##### 1- database_converters
Used by Room Library to convert POJOs to json String, to be able to save POJOs in Table
##### 2- database_gateway
Used As Gateway to connect between domain layer and database framework layer
##### 3- network_gateway
Used As Gateway to connect between domain layer and network layer
##### 4- FixtureLoader
Fixture Loader use case
This use case apply required business num 3 (The list should be sectioned by day)
It Implements Function (RxJava),
Input PremierLeagueApi implementation of Network Gateway
Return Observable <Pair <String, Iterable <MatchesItem>>>
##### 5- SearchItem
Search for nearest date use case
This use case apply business num 4
It Implements Function2 (Kotlin)
Inputs
 1. Pairs list List <Pair <String, List <MatchesItem>>>
 2. Target Date. 
Return index of closet date
##### 6- TeamsLoader
PL teams load use case
It Implements Function (RxJava)
Input PremierLeagueApi implementation of Network Gateway
Return Observable <TeamsItem>


#### Entities
Contain POJOs represent api responses and functional interfaces


## Techical Issues i faced 
1. In `FixtureLoader` Usecase 
groupBy operator is unicast can not used more than once, 
when i try to reload list second time ( configuration changed or reuse list in another fragment  ) app will crash.
**Solution**
Create a new list in observer and reuse it instead of `blockingIterable ()`, something like this
~~~~
 for ( FixturesItem item : stringIterablePair.second ) {
            list.add ( item );
        }
        
~~~~
2. Unit Tests Do not Run!! ( Build Fails )
Error was in Jetifier, and fixed by 
 - make gradle plugin version is 3.2.1 instead of 3.4.0-alpha07 ,This version change needed kotlin version upgrade to 1.2.51
 - add kapt plugin for kotlin, it will not affect apk size, and also make app able to work in kotlin without any problem in future
 

